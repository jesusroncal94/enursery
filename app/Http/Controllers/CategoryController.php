<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Validator;
use DataTables;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::latest()->get();

        return Datatables::of($categories)
            ->addIndexColumn()
            ->addColumn('image_upload', function($row){
                $image_upload = '<img src="'.URL::asset('/images/categories/'.$row->image).'" width="80px" height="50px">';
                return $image_upload;
            })
            ->addColumn('action', function($row){
                $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-sm editCategory"><i class="fa fa-check"></i></a>';
                $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-sm deleteCategory"><i class="fa fa-close"></i></a>';
                return $btn;
            })
            ->rawColumns(['image_upload', 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        if(
            (is_null($input['category_id']) or empty($input['category_id']))
            or (!empty($input['category_id']) and !empty($input['image']))
        )
        {
            if(!empty($input['category_id']) and !empty($input['image']))
            {
                $category = Category::find($input['category_id']);
                File::delete(public_path('images/categories/'.$category->image));
            }

            $validator = Validator::make($request->all(), [
                'catName' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            if ($validator->passes()) {
                $input['image'] = time().'.'.$request->image->extension();
                $request->image->move(public_path('images/categories'), $input['image']);

                $category = Category::updateOrCreate(
                    ['id' => $input['category_id']],
                    ['catName' => $input['catName'], 'image' => $input['image']]
                );
                $success['name'] = $category->catName;

                return $this->sendResponse($success, 'Category saved successfully.');
            }
        } else
        {
            $validator = Validator::make($request->all(), [
                'catName' => 'required',
            ]);

            if ($validator->passes()) {
                $category = Category::updateOrCreate(
                    ['id' => $input['category_id']],
                    ['catName' => $input['catName']]
                );
                $success['name'] = $category->catName;

                return $this->sendResponse($success, 'Category saved successfully.');
            }
        }

        return $this->sendError('Validation Error.', $validator->errors());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);

        return response()->json($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        Category::find($id)->delete();

        return response()->json(['success'=>'Category deleted successfully.']);
    }

    public function get()
    {
        $categories = Category::get();

        return response()->json($categories);
    }
}
