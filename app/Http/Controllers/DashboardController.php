<?php

namespace App\Http\Controllers;

use App\Dashboard;
use App\User;
use App\Order;
use DB;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $year       = isset($request->year) ? $request->year : 2020;
        $month      = isset($request->month) ? $request->month : 6;

        if ($request->userType == 1) {
            $partners = User::select(
                    DB::raw('YEAR(created_at) AS year'),
                    DB::raw("(SELECT COUNT(*) FROM users WHERE userType = 2) AS 'total_nurseries'"),
                    DB::raw("SUM(CASE WHEN MONTH(created_at) = '".$month."' THEN 1 ELSE 0 END) AS 'month'"),
                )
                ->where('userType', 2)
                ->whereYear('created_at', '=', $year)
                ->groupBy(DB::raw('YEAR(created_at)'))
                ->get();

            $orders = Order::select(
                    DB::raw('YEAR(created_at) AS year'),
                    DB::raw("(SELECT COUNT(*) FROM orders WHERE status_id NOT IN (1, 2)) AS 'total_orders'"),
                    DB::raw("(SELECT SUM(total) FROM orders WHERE status_id NOT IN (1, 2)) AS 'total_revenue'"),
                    DB::raw("(SELECT SUM(order_details.productQuantity) FROM order_details INNER JOIN orders ON order_details.order_id = orders.id WHERE orders.status_id NOT IN (1, 2)) AS 'total_cant_products'"),
                    DB::raw("SUM(CASE WHEN MONTH(created_at) = '".$month."' THEN 1 ELSE 0 END) AS 'month'"),
                    DB::raw("SUM(CASE WHEN MONTH(created_at) = '".$month."' THEN total ELSE 0 END) AS 'month_revenue'"),
                    DB::raw("(SELECT SUM(CASE WHEN MONTH(created_at) = '".$month."' THEN order_details.productQuantity ELSE 0 END) FROM order_details) AS 'month_cant_products'")
                )
                ->whereNotIn('status_id', [1, 2])
                ->whereYear('created_at', '=', $year)
                ->groupBy(DB::raw('YEAR(created_at)'))
                ->get();
        } elseif ($request->userType == 2 and isset($request->partner_id)) {
            $partners = User::select(
                    DB::raw('YEAR(created_at) AS year'),
                    DB::raw("(SELECT COUNT(*) FROM users WHERE userType = 2) AS 'total_nurseries'"),
                    DB::raw("SUM(CASE WHEN MONTH(created_at) = '".$month."' THEN 1 ELSE 0 END) AS 'month'"),
                )
                ->where('userType', 2)
                ->whereYear('created_at', '=', $year)
                ->groupBy(DB::raw('YEAR(created_at)'))
                ->get();

            $orders = Order::select(
                    DB::raw('YEAR(created_at) AS year'),
                    DB::raw("(SELECT COUNT(*) FROM orders WHERE partner_id = '".$request->partner_id."' AND status_id NOT IN (1, 2)) AS 'total_orders'"),
                    DB::raw("(SELECT SUM(order_details.productQuantity) FROM order_details INNER JOIN orders ON order_details.order_id = orders.id WHERE orders.partner_id = '".$request->partner_id."' AND orders.status_id NOT IN (1, 2)) AS 'total_cant_products'"),
                    DB::raw("SUM(CASE WHEN MONTH(created_at) = '".$month."' THEN 1 ELSE 0 END) AS 'month'"),
                    DB::raw("(SELECT SUM(CASE WHEN MONTH(order_details.created_at) = '".$month."' THEN order_details.productQuantity ELSE 0 END) FROM order_details INNER JOIN orders ON order_details.order_id = orders.id WHERE orders.partner_id = '".$request->partner_id."' AND orders.status_id NOT IN (1, 2)) AS 'month_cant_products'")
                )
                ->where('partner_id', $request->partner_id)
                ->whereNotIn('status_id', [1, 2])
                ->whereYear('created_at', '=', $year)
                ->groupBy(DB::raw('YEAR(created_at)'))
                ->get();
        }


        $success['partners']  = $partners;
        $success['orders']    = $orders;

        return $this->sendResponse($success, 'Dashboard data.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function show(Dashboard $dashboard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function edit(Dashboard $dashboard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dashboard $dashboard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dashboard $dashboard)
    {
        //
    }

    public function getMonthName($monthNumber)
    {
        return date("F", mktime(0, 0, 0, $monthNumber, 1));
    }
}
