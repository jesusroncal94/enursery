<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;
use \App\Product;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::latest()->get();
        $products = Product::
            where('status_id', 1)
            ->with([
                'stock' => function ($query) {
                    $query->where('productQuantity', '>', 0);
            }])
            ->latest('id')->take(6)->get();

        return view('user.main', compact('categories', 'products'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function products()
    {
        $products = Product::with('category')->latest()->get();

        return view('product.main', compact('products'));
    }
}
