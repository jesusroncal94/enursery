<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderDetail;
use App\Product;
use App\Stock;
use App\User;
use DataTables;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->userType == 1)
        {
            if($request->view == 'neworders')
            {
                $statuses = [3, 4, 5, 6];
                $orders = Order::whereIn('status_id', $statuses)
                    ->with(['orderDetails', 'user', 'partner', 'status'])
                    ->latest('created_at')->get();
            } elseif($request->view == 'orders') {
                $statuses = [6];
                $orders = Order::whereIn('status_id', $statuses)
                    ->with(['orderDetails', 'user', 'partner', 'status'])
                    ->latest('created_at')->get();
            }
        } elseif($request->userType == 2 and isset($request->partner_id))
        {
            if($request->view == 'neworders')
            {
                $orders = Order::where(function($query) use($request) {
                        $query->where(function($query) use($request) {
                            $query->where('status_id', '=', 4);
                        });
                        $query->orWhere(function($query) use($request) {
                            $query->where('partner_id', '=', $request->partner_id);
                            $query->where('status_id', '=', 5);
                        });
                    })
                    ->with(['orderDetails', 'user', 'partner', 'status'])
                    ->latest('created_at')->get();
            } elseif($request->view == 'orders') {
                $statuses = [6];
                $orders = Order::where('partner_id', $request->partner_id)
                    ->whereIn('status_id', $statuses)
                    ->with(['orderDetails', 'user', 'partner', 'status'])
                    ->latest('created_at')->get();
            }
        } elseif($request->userType == 3 and isset($request->user_id))
        {
            if($request->view == 'orders')
            {
                $statuses = [3, 4, 5, 6];
                $orders = Order::whereIn('status_id', $statuses)
                    ->where('user_id', $request->user_id)
                    ->with(['orderDetails', 'user', 'partner', 'status'])
                    ->latest('created_at')->get();
            }
        }

        return Datatables::of($orders)
            ->addIndexColumn()
            ->addColumn('idFormat', function($row) {
                return 'Ord-'.str_pad($row->id, 6, "0", STR_PAD_LEFT);
            })
            ->addColumn('action', function($row) use($request){
                $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="View" class="view btn btn-sm viewOrder"><i class="fa fa-eye"></i></a>';

                if($request->userType == 1)
                {
                    if($row->status_id == 3)
                    {
                        $btn .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Approve" class="btn btn-sm approveOrder"><i class="fa fa-check"></i></a>';
                    }
                    $btn .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Cancel" class="btn-sm cancelOrder"><i class="fa fa-close"></i></a>';
                } elseif ($request->userType == 2) {
                    if($row->status_id == 4)
                    {
                        $btn .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Process" class="btn btn-sm processOrder"><i class="fa fa-cog"></i></a>';
                    } elseif($row->status_id == 5)
                    {
                        $btn .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delivere" class="btn btn-sm delivereOrder"><i class="fa fa-calendar-check-o"></i></a>';
                    }
                }

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = $request->order[0];
        $order['personalDetails'] = json_encode($request->order[0]['personalDetails'][0]);
        $orderDetails = $request->orderDetails;

        try {
            DB::beginTransaction();
                $orderCreated = Order::create($order);
                foreach ($orderDetails as $orderDetail) {
                    $new_orderDetail = array_merge($orderDetail, array('order_id' => $orderCreated->id));
                    OrderDetail::create($new_orderDetail);
                }
                $user = User::updateOrCreate(
                    ['id' => $request->order[0]['user_id']],
                    ['cart' => null]
                );
            DB::commit();
            $success['order'] = 'Ord-'.str_pad($orderCreated->id, 6, "0", STR_PAD_LEFT).', Usuario: '.$user->name;

            return $this->sendResponse($success, 'Order created successfully.');
        } catch (Exception $e) {
            DB::rollback();

            return $this->sendError('Error.', array($e));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::with(['orderDetails', 'user', 'status'])
            ->find($id);

        return response()->json($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function approveOrder(Request $request)
    {
        try {
            DB::beginTransaction();
                $order = Order::with(['orderDetails'])
                    ->find($request->order_id);
                $order->status_id = 4;
                $order->approved_at = Carbon::now();
                $order->save();
            DB::commit();
            $success['order'] = 'Ord-'.str_pad($order->id, 6, "0", STR_PAD_LEFT);

            return $this->sendResponse($success, 'Order approved successfully.');
        } catch (Exception $e) {
            DB::rollback();

            return $this->sendError('Error.', array($e));
        }
    }

    public function processOrder(Request $request)
    {
        try {
            DB::beginTransaction();
                $order = Order::with(['orderDetails'])
                    ->find($request->order_id);
                $order->status_id = 5;
                $order->partner_id = $request->user_id;
                $order->processed_at = Carbon::now();
                $order->save();
                $orderDetails = OrderDetail::where('order_id', $order->id)
                    ->get();

                foreach($orderDetails as $orderDetail) {
                    $stock = Stock::where([
                            ['product_id', '=', $orderDetail->product_id],
                            ['user_id', '=', $request->user_id],
                        ])
                        ->first();
                    if(isset($stock->productQuantity) and $stock->productQuantity > 0 and $orderDetail->productQuantity < $stock->productQuantity)
                    {
                        $stock->productQuantity = $stock->productQuantity - $orderDetail->productQuantity;
                        $stock->save();
                    } else
                    {
                        DB::rollback();
                        $product = Product::find($orderDetail->product_id);
                        return $this->sendError('Error.', array('Stock' => 'Product '.$product->productName.' does not have enough stock to cover the order.'));
                    }
                }
            DB::commit();
            $success['order'] = 'Ord-'.str_pad($order->id, 6, "0", STR_PAD_LEFT);

            return $this->sendResponse($success, 'Order processed successfully.');
        } catch (Exception $e) {
            DB::rollback();

            return $this->sendError('Error.', array($e));
        }
    }

    public function delivereOrder(Request $request)
    {
        try {
            DB::beginTransaction();
                $order = Order::with(['orderDetails'])
                    ->find($request->order_id);
                $order->status_id = 6;
                $order->delivered_at = Carbon::now();
                $order->save();
            DB::commit();
            $success['order'] = 'Ord-'.str_pad($order->id, 6, "0", STR_PAD_LEFT);

            return $this->sendResponse($success, 'Order delivered successfully.');
        } catch (Exception $e) {
            DB::rollback();

            return $this->sendError('Error.', array($e));
        }
    }
}
