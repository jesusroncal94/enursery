<?php

namespace App\Http\Controllers;

use App\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
   
    public function partners(){
        return $this->belongsTo(User::class);
    }
}
