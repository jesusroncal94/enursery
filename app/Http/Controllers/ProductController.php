<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Validator;
use DataTables;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        (is_null($request->productUser_id) and is_null($request->status_id))
            ? $products = Product::with(['category', 'user', 'stock'])->latest()->get()
            : $products = Product::where('status_id', $request->status_id)
                ->with([
                    'category', 'user', 'stock' => function ($query) use ($request){
                        $query->where('user_id', '=', $request->productUser_id);
                }])
                ->latest()->get();

        return Datatables::of($products)
            ->addIndexColumn()
            ->addColumn('productImage_upload', function($row){
                $productImage_upload = '<img src="'.URL::asset('/images/products/'.$row->productImage).'" width="80px" height="50px">';
                return $productImage_upload;
            })
            ->addColumn('action', function($row){
                $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-sm editProduct"><i class="fa fa-check"></i></a>';
                $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-sm deleteProduct"><i class="fa fa-close"></i></a>';
                return $btn;
            })
            ->rawColumns(['productImage_upload', 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($input = $request->all());
        $input = $request->all();

        if(
            (is_null($input['product_id']) or empty($input['product_id']))
            or (!empty($input['product_id']) and !empty($input['productImage']))
        )
        {
            if(!empty($input['product_id']) and !empty($input['productImage']))
            {
                $product = Product::find($input['product_id']);
                File::delete(public_path('images/products/'.$product->image));
            }

            $validator = Validator::make($request->all(), [
                'productName' => 'required',
                'productDescription' => 'required',
                'productQuantity' => 'required',
                'productPrice' => 'required',
                'productCategory' => 'required',
                'productImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            if ($validator->passes()) {
                $input['productImage'] = time().'.'.$request->productImage->extension();
                $request->productImage->move(public_path('images/products'), $input['productImage']);

                $product = Product::updateOrCreate(
                    ['id' => $input['product_id']],
                    [
                        'productName' => $input['productName'],
                        'productDescription' => $input['productDescription'],
                        'productPrice' => $input['productPrice'],
                        'productCategory' => $input['productCategory'],
                        'productImage' => $input['productImage'],
                        'productUser_id' => $input['productUser_id'],
                        'status_id' => $input['productStatus_id'],
                    ]
                );
                $product->stock()->syncWithoutDetaching([
                    $input['productUser_id'] => ['productQuantity' => $input['productQuantity']],
                ]);
                $success['name'] = $product->productName;

                return $this->sendResponse($success, 'Product saved successfully.');
            }
        } else
        {
            $validator = Validator::make($request->all(), [
                'productName' => 'required',
                'productDescription' => 'required',
                'productQuantity' => 'required',
                'productPrice' => 'required',
                'productCategory' => 'required',
            ]);

            if ($validator->passes()) {
                $product = Product::updateOrCreate(
                    ['id' => $input['product_id']],
                    [
                        'productName' => $input['productName'],
                        'productDescription' => $input['productDescription'],
                        'productPrice' => $input['productPrice'],
                        'productCategory' => $input['productCategory'],
                        'productUser_id' => $input['productUser_id'],
                        'status_id' => $input['productStatus_id'],
                    ]
                );
                $product->stock()->syncWithoutDetaching([
                    $input['productUser_id'] => ['productQuantity' => $input['productQuantity']],
                ]);
                $success['name'] = $product->productName;

                return $this->sendResponse($success, 'Product saved successfully.');
            }
        }

        return $this->sendError('Validation Error.', $validator->errors());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $product = Product::with([
                'stock' => function ($query) use ($request){
                    $query->where('user_id', '=', $request->user_id);
            }])
            ->find($id);

        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        Product::find($id)->delete();

        return response()->json(['success'=>'Product deleted successfully.']);
    }

    public function get()
    {
        $products = Product::with('category')->get();

        return response()->json($products);
    }
}
