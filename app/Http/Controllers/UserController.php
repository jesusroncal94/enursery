<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DataTables;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        (is_null($request->userType) and is_null($request->status_id))
        ?
            $users = User::with(['role', 'status'])->latest()->get()
        :
            $users = User::where([
                  ['userType', $request->userType],
                  ['status_id', $request->status_id],
              ])
              ->with(['role', 'status'])->latest()->get();

        return Datatables::of($users)
            ->addIndexColumn()
            ->addColumn('image_upload', function($row){
                (!is_null($row->image))
                    ? $image_upload = '<img src="'.URL::asset('/images/users/'.$row->image).'" width="50px" height="55px">'
                    : $image_upload = 'No image';
                return $image_upload;
            })
            ->addColumn('action', function($row){
                $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-sm editUser"><i class="fa fa-check"></i></a>';
                $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-sm deleteUser"><i class="fa fa-close"></i></a>';
                return $btn;
            })
            ->rawColumns(['image_upload', 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validate = [
            'name'          => 'required',
            'userType'      => 'required',
            'address'       => 'required',
            'contactNumber' => 'required',
            'status_id'     => 'required',
        ];

        if(!empty($input['user_id']) and !empty($input['email']) and !empty($input['userName']))
        {
            array_merge($validate, array('email' => 'required|email|unique:users,email', 'userName' => 'required|between:8,15|unique:users,userName'));
        } else
        {
            array_merge($validate, array('email' => 'required|email', 'userName' => 'required|between:8,15'));
        }

        // Cases of validation
        if(!empty($input['user_id']) and !empty($input['password']) and !is_null($request->image))
        {
            array_merge($validate, array('password' => 'required', 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'));
            $input = $request->only(['name', 'email', 'userName', 'userType', 'address', 'password', 'contactNumber', 'image', 'status_id']);
        } elseif(
            (empty($input['user_id']) and !empty($input['password']) and is_null($request->image))
            or (!empty($input['user_id']) and !empty($input['password']) and is_null($request->image))
        )
        {
            array_merge($validate, array('password' => 'required'));
            $input = $request->only(['name', 'email', 'userName', 'userType', 'address', 'password', 'contactNumber', 'status_id']);
        } elseif(!empty($input['user_id']) and empty($input['password']) and !is_null($request->image))
        {
            array_merge($validate, array('image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'));
            $input = $request->only(['name', 'email', 'userName', 'userType', 'address', 'contactNumber', 'image', 'status_id']);
        } else
        {
            $validate = $validate;
            $input = $request->only(['name', 'email', 'userName', 'userType', 'address', 'contactNumber', 'status_id']);
        }

        // Send Error if validator fails
        $validator = Validator::make($input, $validate);
        if($validator->fails())
        {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if(!is_null($request->user_id) and !is_null($request->image))
        {
            $user = User::find($request->user_id);
            (!is_null($user->image)) ? File::delete(public_path('images/users/'.$user->image)) : '';
            $input['image'] = time().'.'.$request->image->extension();
            $request->image->move(public_path('images/users'), $input['image']);
        }

        $user = User::updateOrCreate(
            ['id' => $request->user_id],
            $input
        );
        $success['name'] = $user->name;

        return $this->sendResponse($success, 'User saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with(['role', 'status'])->find($id);

        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        (!is_null($user->image)) ? File::delete(public_path('images/users/'.$user->image)) : '';
        $user->delete();
        $success['name'] = $user->name;

        return $this->sendResponse($success, 'User deleted successfully.');
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $identifier = filter_var($request->identifier, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if (Auth::attempt([$identifier => $request->identifier, 'password' => $request->password]) and Auth::user()->status_id == 1)
        {
            $user = Auth::user();
            //$success['token'] = $user->createToken('MyApp')->accessToken;
            $success['name'] = $user->name;

            return $this->sendResponse($success, 'User login successfully.');
        } else {
            Auth::logout();

            return $this->sendError('Unauthorised.', ['error' => 'Unauthorised']);
        }
    }

    /**
     * Logout api
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {
        Auth::logout();

        return redirect('/');
    }


    public function updateCart(Request $request) {
        //dd($request->all());
        $user = User::updateOrCreate(
            ['id' => $request->user_id],
            ['cart' => $request->cart]
        );
    }

    public function loadCart($id) {
        //dd($request->all());
        $user = User::find($id);

        return response()->json($user->cart);
    }
}
