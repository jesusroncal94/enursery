<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'subtotal', 'shipment', 'total', 'personalDetails', 'status_id', 'approved_at', 'processed_at', 'delivered_at',
    ];

    public function orderDetails()
    {
        return $this->belongsToMany(\App\Product::class, 'order_details', 'order_id', 'product_id')
            ->withPivot('productPrice', 'productTotalPrice', 'productQuantity')
            ->withTimeStamps();
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function partner()
    {
        return $this->belongsTo(\App\User::class, 'partner_id');
    }

    public function status()
    {
        return $this->belongsTo(\App\Status::class);
    }

    // public function getIdAttribute()
    // {
    //     $idFormat = 'Ord-'.str_pad($this->attributes['id'], 6, "0", STR_PAD_LEFT);
    //     return $idFormat;
    // }
    //
    // public function setIdAttribute($id)
    // {
    //     $idWithoutFormat = intval(substr($id, -6));
    //     $this->attributes['id'] = $idWithoutFormat;
    // }

    public function getCreatedAtAttribute()
    {
        $date = new DateTime($this->attributes['created_at']);
        return $date->format('Y-m-d H:i:s');
    }

    public function getUpdatedAtAttribute()
    {
        $date = new DateTime($this->attributes['updated_at']);
        return $date->format('Y-m-d H:i:s');
    }

    // public function getApprovedAtAttribute()
    // {
    //     $date = new DateTime($this->attributes['approved_at']);
    //     return $date->format('Y-m-d H:i:s');
    // }
    //
    // public function getProcessedAtAttribute()
    // {
    //     $date = new DateTime($this->attributes['processed_at']);
    //     return $date->format('Y-m-d H:i:s');
    // }
    //
    // public function getDeliveredAtAttribute()
    // {
    //     $date = new DateTime($this->attributes['delivered_at']);
    //     return $date->format('Y-m-d H:i:s');
    // }
}
