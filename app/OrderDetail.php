<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'product_id', 'productPrice', 'productTotalPrice', 'productQuantity',
    ];

    // public function getOrderIdAttribute()
    // {
    //     $order_idFormat = 'Ord-'.str_pad($this->attributes['order_id'], 6, "0", STR_PAD_LEFT);
    //     return $order_idFormat;
    // }
    // 
    // public function setOrderIdAttribute($order_id)
    // {
    //     $order_idWithoutFormat = intval(substr($order_id, -6));
    //     $this->attributes['order_id'] = $order_idWithoutFormat;
    // }
}
