<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'productName', 'productDescription', 'productPrice', 'productCategory', 'productImage', 'productUser_id', 'status_id'
    ];

    public function category()
    {
        return $this->belongsTo(\App\Category::class, 'productCategory');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'productUser_id');
    }

    public function stock()
    {
        return $this->belongsToMany(\App\User::class, 'stocks', 'product_id', 'user_id')->withPivot('productQuantity')->withTimeStamps();
    }

    public function status()
    {
        return $this->belongsTo(\App\Status::class);
    }

    public function getCreatedAtAttribute()
    {
        $date = new DateTime($this->attributes['created_at']);
        return $date->format('Y-m-d H:i:s');
    }

    public function getUpdatedAtAttribute()
    {
        $date = new DateTime($this->attributes['updated_at']);
        return $date->format('Y-m-d H:i:s');
    }
}
