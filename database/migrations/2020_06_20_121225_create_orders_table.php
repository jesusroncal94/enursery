<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->unsignedInteger('subtotal');
            $table->unsignedInteger('shipment');
            $table->unsignedInteger('total');
            $table->json('personalDetails');
            $table->bigInteger('partner_id');
            $table->string('status_id');
            $table->timestamps();
            $table->dateTime('approved_at');
            $table->dateTime('processed_at');
            $table->dateTime('delivered_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
