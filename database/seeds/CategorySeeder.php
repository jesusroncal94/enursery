<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'catName' => 'Plants',
                'image'   => 'plants.png'
            ],
            [
                'catName' => 'Flowers',
                'image'   => 'flowers.png'
            ],
            [
                'catName' => 'Trees',
                'image'   => 'trees.png'
            ],
        ];

        foreach ($categories as $category) {
            Category::create($category);
        }
    }
}
