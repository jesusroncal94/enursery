<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'productName'         => 'Plant 1',
                'productDescription'  => 'ABC',
                'productPrice'        => 10,
                'productCategory'     => 1,
                'productImage'        => 'plant1.png',
                'productUser_id'      => 2,
                'status_id'           => 1,
            ],
            [
                'productName'         => 'Plant 2',
                'productDescription'  => 'DEF',
                'productPrice'        => 20,
                'productCategory'     => 1,
                'productImage'        => 'plant2.png',
                'productUser_id'      => 3,
                'status_id'           => 1,
            ],
            [
                'productName'         => 'Flower 1',
                'productDescription'  => 'FGH',
                'productPrice'        => 30,
                'productCategory'     => 2,
                'productImage'        => 'flower1.png',
                'productUser_id'      => 2,
                'status_id'           => 1,
            ],
            [
                'productName'         => 'Flower 2',
                'productDescription'  => 'IJK',
                'productPrice'        => 40,
                'productCategory'     => 2,
                'productImage'        => 'flower2.png',
                'productUser_id'      => 3,
                'status_id'           => 1,
            ],
            [
                'productName'         => 'Tree 1',
                'productDescription'  => 'LMN',
                'productPrice'        => 50,
                'productCategory'     => 3,
                'productImage'        => 'tree1.png',
                'productUser_id'      => 2,
                'status_id'           => 1,
            ],
            [
                'productName'         => 'Tree 2',
                'productDescription'  => 'OPQ',
                'productPrice'        => 60,
                'productCategory'     => 3,
                'productImage'        => 'tree2.png',
                'productUser_id'      => 3,
                'status_id'           => 1,
            ],
        ];

        foreach ($products as $product) {
            Product::create($product);
        }
    }
}
