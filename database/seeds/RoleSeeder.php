<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name'          => 'admin',
                'display_name'  => 'Admin'
            ],
            [
                'name'          => 'partner',
                'display_name'  => 'Partner',
            ],
            [
                'name'          => 'customer',
                'display_name'  => 'Customer',
            ],
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }
    }
}
