<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            [
                'name'          => 'active',
                'display_name'  => 'Active'
            ],
            [
                'name'          => 'inactive',
                'display_name'  => 'Inactive',
            ],
            [
                'name'          => 'pending',
                'display_name'  => 'Pending',
            ],
            [
                'name'          => 'approved',
                'display_name'  => 'Approved',
            ],
            [
                'name'          => 'processing'
                'display_name'  => 'Processing'
            ],
            [
                'name'          => 'delivered',
                'display_name'  => 'Delivered',
            ],
        ];

        foreach ($statuses as $status) {
            Status::create($status);
        }
    }
}
