<?php

use Illuminate\Database\Seeder;
use \App\Stock;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stock = [
            [
                'product_id'      => 1,
                'user_id'         => 2,
                'productQuantity' => 10,
            ],
            [
                'product_id'      => 2,
                'user_id'         => 2,
                'productQuantity' => 20,
            ],
            [
                'product_id'      => 3,
                'user_id'         => 2,
                'productQuantity' => 10,
            ],
            [
                'product_id'      => 4,
                'user_id'         => 2,
                'productQuantity' => 20,
            ],
            [
                'product_id'      => 5,
                'user_id'         => 2,
                'productQuantity' => 10,
            ],
            [
                'product_id'      => 6,
                'user_id'         => 2,
                'productQuantity' => 20,
            ],
            [
                'product_id'      => 1,
                'user_id'         => 3,
                'productQuantity' => 30,
            ],
            [
                'product_id'      => 2,
                'user_id'         => 3,
                'productQuantity' => 40,
            ],
            [
                'product_id'      => 3,
                'user_id'         => 3,
                'productQuantity' => 30,
            ],
            [
                'product_id'      => 4,
                'user_id'         => 3,
                'productQuantity' => 40,
            ],
            [
                'product_id'      => 5,
                'user_id'         => 3,
                'productQuantity' => 30,
            ],
            [
                'product_id'      => 6,
                'user_id'         => 3,
                'productQuantity' => 40,
            ],
        ];

        foreach ($stock as $stockItem) {
            Stock::create($stockItem);
        }
    }
}
