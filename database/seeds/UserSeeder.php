<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminUser = [
            'name'          => 'Super Admin',
            'email'         => 'superadmin@gmail.com',
            'userName'      => 'superadmin',
            'userType'      => 1,
            'address'       => "Av. Test",
            'password'      => 'enursery2020',
            'contactNumber' => '+92 304 1234567',
            'image'         => 'superadmin.png',
            'status_id'     => 1,
        ];

        $partnerUser1 = [
            'name'          => 'Jesus Roncal',
            'email'         => 'jesusroncal94@gmail.com',
            'userName'      => 'jesusroncal94',
            'userType'      => 2,
            'address'       => "Av. Test",
            'password'      => 'roncal94',
            'contactNumber' => '+51 971905162',
            'image'         => 'partner1.jpeg',
            'status_id'     => 1,
        ];

        $partnerUser2 = [
            'name'          => 'Ali Jawad',
            'email'         => 'alijawad20@gmail.com',
            'userName'      => 'alijawad20',
            'userType'      => 2,
            'address'       => "Av. Test",
            'password'      => 'jawad20',
            'contactNumber' => '+92 304 3040955',
            'image'         => 'partner2.jpg',
            'status_id'     => 1,
        ];
        User::create($adminUser);
        User::create($partnerUser1);
        User::create($partnerUser2);
    }
}
