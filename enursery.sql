-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 22-06-2020 a las 02:45:04
-- Versión del servidor: 5.7.30-0ubuntu0.18.04.1
-- Versión de PHP: 7.3.19-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `enursery`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `catName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `catName`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Plants', 'plants.png', '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(2, 'Flowers', 'flowers.png', '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(3, 'Trees', 'trees.png', '2020-06-20 15:24:59', '2020-06-20 15:24:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(112, '2014_10_12_000000_create_users_table', 1),
(113, '2014_10_12_100000_create_password_resets_table', 1),
(114, '2019_08_19_000000_create_failed_jobs_table', 1),
(115, '2020_06_13_094737_create_roles_table', 1),
(116, '2020_06_13_100041_create_statuses_table', 1),
(117, '2020_06_14_145026_create_categories_table', 1),
(118, '2020_06_14_180806_create_products_table', 1),
(119, '2020_06_18_110426_create_stocks_table', 1),
(120, '2020_06_20_121225_create_orders_table', 1),
(121, '2020_06_20_133003_create_order_details_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `subtotal` int(10) UNSIGNED NOT NULL,
  `shipment` int(10) UNSIGNED NOT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `personalDetails` json NOT NULL,
  `partner_id` bigint(20) DEFAULT NULL,
  `status_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `processed_at` datetime DEFAULT NULL,
  `delivered_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `subtotal`, `shipment`, `total`, `personalDetails`, `partner_id`, `status_id`, `created_at`, `updated_at`, `approved_at`, `processed_at`, `delivered_at`) VALUES
(1, 2, 80, 25, 105, '{\"name\": \"Jesus Roncal\", \"email\": \"jesusroncal94@gmail.com\", \"address\": \"Av. Test\", \"contactNumber\": \"+51 971905162\"}', NULL, '3', '2020-06-20 15:31:43', '2020-06-20 15:31:43', NULL, NULL, NULL),
(2, 2, 110, 25, 135, '{\"name\": \"Jesus Roncal\", \"email\": \"jesusroncal94@gmail.com\", \"address\": \"Av. Test\", \"contactNumber\": \"+51 971905162\"}', NULL, '3', '2020-06-20 16:02:53', '2020-06-20 16:02:53', NULL, NULL, NULL),
(3, 5, 260, 25, 285, '{\"name\": \"Kaisy\", \"email\": \"alvaradokaisy@gmail.com\", \"address\": \"Av.xxxx\", \"contactNumber\": \"999999999\"}', 2, '6', '2020-06-20 16:11:47', '2020-06-21 23:42:18', '2020-06-21 00:00:00', '2020-06-21 00:00:00', '2020-06-21 00:00:00'),
(4, 6, 220, 25, 245, '{\"name\": \"AliJawad11\", \"email\": \"alijawad11@live.com\", \"address\": \"Nadala\", \"contactNumber\": \"02112345\"}', NULL, '3', '2020-06-20 16:15:19', '2020-06-20 16:15:19', NULL, NULL, NULL),
(5, 1, 60, 25, 85, '{\"name\": \"Super Admin\", \"email\": \"superadmin@gmail.com\", \"address\": \"Av. Test\", \"contactNumber\": \"+92 304 1234567\"}', NULL, '3', '2020-06-20 18:03:13', '2020-06-20 18:03:13', NULL, NULL, NULL),
(6, 1, 60, 25, 85, '{\"name\": \"Super Admin\", \"email\": \"superadmin@gmail.com\", \"address\": \"Av. Test\", \"contactNumber\": \"+92 304 1234567\"}', 3, '6', '2020-06-20 18:03:38', '2020-06-22 02:24:56', '2020-06-22 01:59:04', '2020-06-22 01:59:46', '2020-06-22 02:24:56'),
(7, 8, 120, 25, 145, '{\"name\": \"Grecia Yucra\", \"email\": \"yucra.grecia@gmail.com\", \"address\": \"LAS PALMAS EDIF E1 DPTO 5O5 CASA CLUB EL PÁLMAR\", \"contactNumber\": \"940513052\"}', 2, '6', '2020-06-20 21:34:22', '2020-06-21 23:58:38', '2020-06-21 00:00:00', '2020-06-21 23:57:48', '2020-06-21 23:58:38'),
(8, 3, 150, 25, 175, '{\"name\": \"Ali Jawad\", \"email\": \"alijawad20@gmail.com\", \"address\": \"Av. Test\", \"contactNumber\": \"+92 304 3040955\"}', 3, '6', '2020-06-21 06:53:12', '2020-06-22 02:24:51', '2020-06-22 01:59:21', '2020-06-22 01:59:43', '2020-06-22 02:24:51'),
(9, 1, 110, 25, 135, '{\"name\": \"Super Admin\", \"email\": \"superadmin@gmail.com\", \"address\": \"Av. Test\", \"contactNumber\": \"+92 304 1234567\"}', 3, '6', '2020-06-21 07:55:08', '2020-06-22 01:02:15', '2020-06-21 00:00:00', '2020-06-21 00:00:00', '2020-06-22 01:02:15'),
(10, 3, 100, 25, 125, '{\"name\": \"Ali Jawad\", \"email\": \"alijawad20@gmail.com\", \"address\": \"Av. Test\", \"contactNumber\": \"+92 304 3040955\"}', 3, '6', '2020-06-21 09:14:06', '2020-06-22 01:58:13', '2020-06-21 23:56:27', '2020-06-22 01:58:06', '2020-06-22 01:58:13'),
(11, 9, 60, 25, 85, '{\"name\": \"Jose Luis Bautista\", \"email\": \"jlbautistavelasquez@gmail.com\", \"address\": \"Jr. Simón Bolivar 324\", \"contactNumber\": \"995710850\"}', 3, '6', '2020-06-21 09:46:06', '2020-06-22 01:01:51', '2020-06-21 00:00:00', '2020-06-21 00:00:00', '2020-06-22 01:01:51'),
(16, 2, 1120, 25, 1145, '{\"name\": \"Jesus Roncal\", \"email\": \"jesusroncal94@gmail.com\", \"address\": \"Av. Test\", \"contactNumber\": \"+51 971905162\"}', 2, '6', '2020-06-21 11:39:41', '2020-06-22 00:42:58', '2020-06-21 00:00:00', '2020-06-21 00:00:00', '2020-06-22 00:42:58'),
(17, 2, 160, 25, 185, '{\"name\": \"Jesus Roncal\", \"email\": \"jesusroncal94@gmail.com\", \"address\": \"Av. Test\", \"contactNumber\": \"+51 971905162\"}', 3, '6', '2020-06-22 00:18:10', '2020-06-22 01:45:25', '2020-06-22 00:18:32', '2020-06-22 01:45:11', '2020-06-22 01:45:25'),
(18, 3, 250, 25, 275, '{\"name\": \"Ali Jawad\", \"email\": \"alijawad20@gmail.com\", \"address\": \"Av. Test\", \"contactNumber\": \"+92 304 3040955\"}', 3, '6', '2020-06-22 01:04:11', '2020-06-22 01:12:14', '2020-06-22 01:08:52', '2020-06-22 01:10:41', '2020-06-22 01:12:14'),
(19, 10, 300, 25, 325, '{\"name\": \"Ajs20\", \"email\": \"ajs220@gm.com\", \"address\": \"Cmplhfd\", \"contactNumber\": \"03125684258\"}', 3, '6', '2020-06-22 02:05:30', '2020-06-22 02:11:42', '2020-06-22 02:06:12', '2020-06-22 02:11:34', '2020-06-22 02:11:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_details`
--

CREATE TABLE `order_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `productPrice` int(10) UNSIGNED NOT NULL,
  `productTotalPrice` int(10) UNSIGNED NOT NULL,
  `productQuantity` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `product_id`, `productPrice`, `productTotalPrice`, `productQuantity`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 10, 10, 1, '2020-06-20 15:31:43', '2020-06-20 15:31:43'),
(2, 1, 2, 20, 20, 1, '2020-06-20 15:31:43', '2020-06-20 15:31:43'),
(3, 1, 5, 50, 50, 1, '2020-06-20 15:31:43', '2020-06-20 15:31:43'),
(4, 2, 5, 50, 50, 1, '2020-06-20 16:02:53', '2020-06-20 16:02:53'),
(5, 2, 6, 60, 60, 1, '2020-06-20 16:02:53', '2020-06-20 16:02:53'),
(6, 3, 1, 10, 20, 2, '2020-06-20 16:11:47', '2020-06-20 16:11:47'),
(7, 3, 6, 60, 240, 4, '2020-06-20 16:11:47', '2020-06-20 16:11:47'),
(8, 4, 4, 40, 160, 4, '2020-06-20 16:15:19', '2020-06-20 16:15:19'),
(9, 4, 6, 60, 60, 1, '2020-06-20 16:15:19', '2020-06-20 16:15:19'),
(10, 5, 6, 60, 60, 1, '2020-06-20 18:03:13', '2020-06-20 18:03:13'),
(11, 6, 6, 60, 60, 1, '2020-06-20 18:03:38', '2020-06-20 18:03:38'),
(12, 7, 6, 60, 120, 2, '2020-06-20 21:34:22', '2020-06-20 21:34:22'),
(13, 8, 7, 50, 50, 1, '2020-06-21 06:53:12', '2020-06-21 06:53:12'),
(14, 8, 8, 100, 100, 1, '2020-06-21 06:53:12', '2020-06-21 06:53:12'),
(15, 9, 7, 50, 50, 1, '2020-06-21 07:55:08', '2020-06-21 07:55:08'),
(16, 9, 6, 60, 60, 1, '2020-06-21 07:55:08', '2020-06-21 07:55:08'),
(17, 10, 8, 100, 100, 1, '2020-06-21 09:14:06', '2020-06-21 09:14:06'),
(18, 11, 6, 60, 60, 1, '2020-06-21 09:46:06', '2020-06-21 09:46:06'),
(19, 16, 6, 60, 540, 9, '2020-06-21 11:39:41', '2020-06-21 11:39:41'),
(20, 16, 5, 50, 150, 3, '2020-06-21 11:39:41', '2020-06-21 11:39:41'),
(21, 16, 1, 10, 20, 2, '2020-06-21 11:39:41', '2020-06-21 11:39:41'),
(22, 16, 2, 20, 200, 10, '2020-06-21 11:39:41', '2020-06-21 11:39:41'),
(23, 16, 3, 30, 210, 7, '2020-06-21 11:39:41', '2020-06-21 11:39:41'),
(24, 17, 8, 100, 100, 1, '2020-06-22 00:18:10', '2020-06-22 00:18:10'),
(25, 17, 6, 60, 60, 1, '2020-06-22 00:18:10', '2020-06-22 00:18:10'),
(26, 18, 7, 50, 50, 1, '2020-06-22 01:04:11', '2020-06-22 01:04:11'),
(27, 18, 8, 100, 200, 2, '2020-06-22 01:04:11', '2020-06-22 01:04:11'),
(28, 19, 8, 100, 200, 2, '2020-06-22 02:05:30', '2020-06-22 02:05:30'),
(29, 19, 7, 50, 100, 2, '2020-06-22 02:05:30', '2020-06-22 02:05:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `productName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productDescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productPrice` int(10) UNSIGNED NOT NULL,
  `productCategory` bigint(20) NOT NULL,
  `productUser_id` bigint(20) NOT NULL,
  `productImage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `productName`, `productDescription`, `productPrice`, `productCategory`, `productUser_id`, `productImage`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Plant 1', 'ABC', 10, 1, 2, 'plant1.png', '1', '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(2, 'Plant 2', 'DEF', 20, 1, 3, 'plant2.png', '1', '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(3, 'Flower 1', 'FGH', 30, 2, 2, 'flower1.png', '1', '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(4, 'Flower 2', 'IJK', 40, 2, 3, 'flower2.png', '1', '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(5, 'Tree 1', 'LMN', 50, 3, 2, 'tree1.png', '1', '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(6, 'Tree 2', 'OPQ', 60, 3, 3, 'tree2.png', '1', '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(7, 'Rose', 'Rose is beautiful.', 50, 2, 2, '1592722139.jpeg', '1', '2020-06-21 06:48:59', '2020-06-22 02:32:28'),
(8, 'Acer Palmatum', 'Is an East Asian art form which utilizes cultivation techniques to produce, in containers, small trees that mimic the shape and scale of full size trees', 100, 1, 2, '1592722291.jpeg', '1', '2020-06-21 06:51:31', '2020-06-22 02:32:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', '2020-06-20 15:24:58', '2020-06-20 15:24:58'),
(2, 'partner', 'Partner', '2020-06-20 15:24:58', '2020-06-20 15:24:58'),
(3, 'customer', 'Customer', '2020-06-20 15:24:58', '2020-06-20 15:24:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `statuses`
--

CREATE TABLE `statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `statuses`
--

INSERT INTO `statuses` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'active', 'Active', '2020-06-20 15:24:58', '2020-06-20 15:24:58'),
(2, 'inactive', 'Inactive', '2020-06-20 15:24:58', '2020-06-20 15:24:58'),
(3, 'pending', 'Pending', '2020-06-21 00:00:00', '2020-06-21 00:00:00'),
(4, 'approved', 'Approved', '2020-06-21 00:00:00', '2020-06-21 00:00:00'),
(5, 'processing', 'Processing', '2020-06-21 00:00:00', '2020-06-21 00:00:00'),
(6, 'delivered', 'Delivered', '2020-06-21 00:00:00', '2020-06-21 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stocks`
--

CREATE TABLE `stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `productQuantity` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `stocks`
--

INSERT INTO `stocks` (`id`, `product_id`, `user_id`, `productQuantity`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 6, '2020-06-20 15:24:59', '2020-06-21 23:32:50'),
(2, 2, 2, 10, '2020-06-20 15:24:59', '2020-06-21 23:32:50'),
(3, 3, 2, 3, '2020-06-20 15:24:59', '2020-06-21 23:32:50'),
(4, 4, 2, 20, '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(5, 5, 2, 7, '2020-06-20 15:24:59', '2020-06-21 23:32:50'),
(6, 6, 2, 5, '2020-06-20 15:24:59', '2020-06-21 23:57:48'),
(7, 1, 3, 30, '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(8, 2, 3, 40, '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(9, 3, 3, 30, '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(10, 4, 3, 40, '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(11, 5, 3, 30, '2020-06-20 15:24:59', '2020-06-20 15:24:59'),
(12, 6, 3, 36, '2020-06-20 15:24:59', '2020-06-22 01:59:46'),
(13, 7, 3, 45, '2020-06-21 06:48:59', '2020-06-22 02:11:34'),
(14, 8, 3, 23, '2020-06-21 06:51:31', '2020-06-22 02:27:23'),
(15, 8, 2, 50, '2020-06-22 02:32:20', '2020-06-22 02:32:20'),
(16, 7, 2, 45, '2020-06-22 02:32:28', '2020-06-22 02:32:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userType` bigint(20) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactNumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cart` json DEFAULT NULL,
  `status_id` bigint(20) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `userName`, `userType`, `address`, `contactNumber`, `email`, `password`, `image`, `cart`, `status_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'superadmin', 1, 'Av. Test', '+92 304 1234567', 'superadmin@gmail.com', '$2y$10$0SVyqcc8HhZhYElet3c8j.sdiDT9fDG4nQDjkWw.RPGQbVHAqlwX6', 'superadmin.png', NULL, 1, NULL, '2020-06-20 15:24:59', '2020-06-21 07:55:08'),
(2, 'Jesus Roncal', 'jesusroncal94', 2, 'Av. Test', '+51 971905162', 'jesusroncal94@gmail.com', '$2y$10$P2UsFErJo83Ay7l9zConx.74By3O0sSSj/e.QVnh8VXHHcuvo9Fge', 'partner1.jpeg', NULL, 1, NULL, '2020-06-20 15:24:59', '2020-06-22 00:18:10'),
(3, 'Ali Jawad', 'alijawad20', 2, 'Av. Test', '+92 304 3040955', 'alijawad20@gmail.com', '$2y$10$Pdhl0lMkaNg4XdG4VN9a6OHLXhbRm4IfnJYS9iG9PfL9AcJnrOb8e', 'partner2.jpg', NULL, 1, NULL, '2020-06-20 15:24:59', '2020-06-22 01:04:11'),
(4, 'yoni', 'elyisus', 3, 'sapo', 'bravo', 'hackingmail@loggrasom.com', '$2y$10$WvLL3/emVXBUY7W1cHy8QevmWpTZwz5iUxdKVfCQUdDSjvutCFRAy', NULL, '[{\"id\": \"6\", \"productName\": \"Tree 2\", \"productImage\": \"tree2.png\", \"productPrice\": \"60\", \"productQuantity\": \"4\", \"productTotalPrice\": \"240\"}, {\"id\": \"4\", \"productName\": \"Flower 2\", \"productImage\": \"flower2.png\", \"productPrice\": \"40\", \"productQuantity\": \"1\", \"productTotalPrice\": \"40\"}, {\"id\": \"2\", \"productName\": \"Plant 2\", \"productImage\": \"plant2.png\", \"productPrice\": \"20\", \"productQuantity\": \"1\", \"productTotalPrice\": \"20\"}]', 1, NULL, '2020-06-20 16:06:53', '2020-06-20 16:29:06'),
(5, 'Kaisy', 'kaiap', 3, 'Av.xxxx', '999999999', 'alvaradokaisy@gmail.com', '$2y$10$yNXKcgsCnO3nkMVJiYYoku9yLo.IAsqAEEkjKeKm4iAXxsoXZWBxS', NULL, NULL, 1, NULL, '2020-06-20 16:10:48', '2020-06-20 16:11:47'),
(6, 'AliJawad11', 'alijawad11', 3, 'Nadala', '02112345', 'alijawad11@live.com', '$2y$10$hvx/1oV7rZaAzlcODAeauuXDe4JKH/y1BeZmqkW1uVH0oOnEWU2E.', NULL, NULL, 1, NULL, '2020-06-20 16:14:14', '2020-06-20 16:15:19'),
(7, 'Rosa Meltrozo', 'Ellechero', 3, 'La lema taita', '00000007', 'lamar39k_u762h@mecip.net', '$2y$10$j2N5lH92Qm2Y4vBp0xVGOepX8DJenfjlqEWH6rMut7RHU0k2bB1l2', NULL, '[{\"id\": \"6\", \"productName\": \"Tree 2\", \"productImage\": \"tree2.png\", \"productPrice\": \"60\", \"productQuantity\": \"5\", \"productTotalPrice\": \"300\"}, {\"id\": \"2\", \"productName\": \"Plant 2\", \"productImage\": \"plant2.png\", \"productPrice\": \"20\", \"productQuantity\": \"1\", \"productTotalPrice\": \"20\"}]', 1, NULL, '2020-06-20 18:11:55', '2020-06-20 18:18:16'),
(8, 'Grecia Yucra', 'greciagris', 3, 'LAS PALMAS EDIF E1 DPTO 5O5 CASA CLUB EL PÁLMAR', '940513052', 'yucra.grecia@gmail.com', '$2y$10$Zqrep9xTV8E3d0uPt64O2eCFnLL.1V1tG3wagHe8l3GEML0cjBefq', NULL, NULL, 1, NULL, '2020-06-20 21:31:59', '2020-06-20 21:34:22'),
(9, 'Jose Luis Bautista', 'JoseBautista', 3, 'Jr. Simón Bolivar 324', '995710850', 'jlbautistavelasquez@gmail.com', '$2y$10$8d/fSdnnREy9JQZ95k9RKufsO.N9p3aUZCLdv4KdrpfylkW7jg5eu', NULL, NULL, 1, NULL, '2020-06-21 09:45:10', '2020-06-21 09:46:06'),
(10, 'Ajs20', 'Ajs22000', 3, 'Cmplhfd', '03125684258', 'ajs220@gm.com', '$2y$10$2rVAdrdrFvkmqzzbnDt8YeriOUw2C/.ez7jxiZlDCoqgZwTqTCiLm', NULL, NULL, 1, NULL, '2020-06-22 02:03:59', '2020-06-22 02:05:30');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_catname_unique` (`catName`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_productname_unique` (`productName`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indices de la tabla `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `statuses_name_unique` (`name`);

--
-- Indices de la tabla `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`userName`),
  ADD UNIQUE KEY `users_contactnumber_unique` (`contactNumber`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
