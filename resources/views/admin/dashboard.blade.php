<div id="dashboard" class="active tab-pane">
    <div class="col-xs-offset-1 col-xs-10" style="">
        <div class="container-fluid">
            <br>
            <div>
                <form id="form_dashboard" class="form-inline">
                    <input type="hidden" name="userType" value="{{ isset(Auth::user()->userType) ? Auth::user()->userType : '' }}">
                    <div class="form-group">
                        <label for="sel1">Year:</label>
                        <select class="form-control" id="dashboard_year" name="year" required>
                            <option value="">Select</option>
                            @php
                                $year_initial = 2020;
                                $year_current = intVal(date('Y'));
                                $diff = (($year_current - $year_initial) == 0) ? 1 : ($year_current - $year_initial + 1);
                            @endphp
                            @for ($i = 1; $i <= $diff; $i++)
                                <option value="{{ $year_initial }}">{{ $year_initial }}</option>
                                @php
                                    $year_initial ++
                                @endphp
                            @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sel1">Month:</label>
                        <select class="form-control" id="dashboard_month" name="month" required>
                            <option value="">Select</option>
                            <option value="1">Enero</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </div>
                    <input type="submit" id="dashboard_btn" class="btn btn-success" value="Submit"></input>
                </form>
            </div>
            <div id="subservs" class="sub-servs" style="display: none">
                <div class="col-xs-12 col-md-6 box ">
                    <a data-toggle="tab" href="#registration">
                        <div class="serv-block box-shadow">
                            <span class="fa fa-handshake-o large pull-right"></span>
                            <h3 class="">
                                Partner Nurseries
                                <small id="dashboard_partners"></small>
                            </h3>
                            <h4>Total Nurseries: <b id="dashboard_partners_total"></b></h4>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-md-6 box">
                    <a data-toggle="tab" href="#newOrder">
                        <div class="serv-block box-shadow">
                            <span class="fa fa-shopping-cart large pull-right"></span>
                            <h3>
                                Order(s)
                                <small id="dashboard_orders"></small>
                            </h3>
                            <h4>
                                Total Orders: <b id="dashboard_orders_total"></b>
                            </h4>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-md-6 box">
                    <a>
                        <div class="serv-block box-shadow">
                            <span class="fa fa-bitcoin large pull-right"></span>
                            <h3>
                                Average Monthly Revenue
                                <small id="dashboard_revenue"></small>
                            </h3>
                            <h4>
                                Total Revenue: <b id="dashboard_revenue_total"></b>
                            </h4>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-md-6 box">
                    <a>
                        <div class="serv-block box-shadow">
                            <span class="fa fa-leaf large pull-right"></span>
                            <h3>
                                Average Monthly Sale
                                <small id="dashboard_sale"></small>
                            </h3>
                            <h4>
                                Total Sale: <b id="dashboard_sale_total"><small> (plants)</small></b>
                            </h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-offset-1 col-xs-10">
        <div class="container-fluid" style="display: none">
            <div class="col-md-6 col-xs-12 col-xs-12">
                <div class="wrapper">
                    <div class="graph-info">
                        <h3> Average&nbsp;Monthly Orders</h3>
                        <h5>
                            <span class="fa fa-arrow-up" style="color:green"><b> 55%</b></span>
                            Inrease in last Compagin.
                        </h5>
                    </div>
                    <canvas id='c'></canvas>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 col-xs-12">
                <div class="wrapper">
                    <div>
                        <div class="graph-info">
                            <h3> Average&nbsp;Monthly Flower Sales</h3>
                            <h5>
                                <span class="fa fa-arrow-up" style="color:green">
                                    <b> 55%</b>
                                </span>
                                Increase in last Compagin.
                            </h5>
                        </div>
                        <section class="bar-graph bar-graph-vertical bar-graph-two">
                            <div class="bar-one bar-container">
                                <div class="bar" data-percentage="30%"></div>
                                <span class="year">Jan</span>
                            </div>
                            <div class="bar-two bar-container">
                                <div class="bar" data-percentage="40%"></div>
                                <span class="year">Feb</span>
                            </div>
                            <div class="bar-three bar-container">
                                <div class="bar" data-percentage="50%"></div>
                                <span class="year">March</span>
                            </div>
                            <div class="bar-four bar-container">
                                <div class="bar" data-percentage="60%"></div>
                                <span class="year">April</span>
                            </div>
                            <div class="bar-five bar-container">
                                <div class="bar" data-percentage="70%"></div>
                                <span class="year">May</span>
                            </div>
                            <div class="bar-six bar-container">
                                <div class="bar" data-percentage="80%"></div>
                                <span class="year">June</span>
                            </div>
                            <div class="bar-seven bar-container">
                                <div class="bar" data-percentage="90%"></div>
                                <span class="year">July</span>
                            </div>
                            <div class="bar-eight bar-container">
                                <div class="bar" data-percentage="100%"></div>
                                <span class="year">Aug</span>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 col-xs-12">
                <div class="wrapper">
                    <div>
                        <div class="graph-info">
                            <h3> Average&nbsp;Monthly Plants Sales</h3>
                            <h5>
                                <span class="fa fa-arrow-up" style="color:green">
                                    <b> 55%</b>
                                </span>
                                Increase in last Compagin.
                            </h5>
                        </div>
                        <section class="bar-graph bar-graph-vertical bar-graph-two">
                            <div class="bar-one bar-container">
                                <div class="bar" data-percentage="30%"></div>
                                <span class="year">Jan</span>
                            </div>
                            <div class="bar-two bar-container">
                                <div class="bar" data-percentage="40%"></div>
                                <span class="year">Feb</span>
                            </div>
                            <div class="bar-three bar-container">
                                <div class="bar" data-percentage="50%"></div>
                                <span class="year">March</span>
                            </div>
                            <div class="bar-four bar-container">
                                <div class="bar" data-percentage="60%"></div>
                                <span class="year">April</span>
                            </div>
                            <div class="bar-five bar-container">
                                <div class="bar" data-percentage="70%"></div>
                                <span class="year">May</span>
                            </div>
                            <div class="bar-six bar-container">
                                <div class="bar" data-percentage="80%"></div>
                                <span class="year">June</span>
                            </div>
                            <div class="bar-seven bar-container">
                                <div class="bar" data-percentage="90%"></div>
                                <span class="year">July</span>
                            </div>
                            <div class="bar-eight bar-container">
                                <div class="bar" data-percentage="100%"></div>
                                <span class="year">Aug</span>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 col-xs-12">
                <div class="wrapper">
                    <div>
                        <div class="graph-info">
                            <h3> Average&nbsp;Monthly Fruit Plants Sales</h3>
                            <h5>
                                <span class="fa fa-arrow-up" style="color:green">
                                    <b> 55%</b>
                                </span>
                                Increase in last Compagin.
                            </h5>
                        </div>
                        <section class="bar-graph bar-graph-vertical bar-graph-two">
                            <div class="bar-one bar-container">
                                <div class="bar" data-percentage="30%"></div>
                                <span class="year">Jan</span>
                            </div>
                            <div class="bar-two bar-container">
                                <div class="bar" data-percentage="40%"></div>
                                <span class="year">Feb</span>
                            </div>
                            <div class="bar-three bar-container">
                                <div class="bar" data-percentage="50%"></div>
                                <span class="year">March</span>
                            </div>
                            <div class="bar-four bar-container">
                                <div class="bar" data-percentage="60%"></div>
                                <span class="year">April</span>
                            </div>
                            <div class="bar-five bar-container">
                                <div class="bar" data-percentage="70%"></div>
                                <span class="year">May</span>
                            </div>
                            <div class="bar-six bar-container">
                                <div class="bar" data-percentage="80%"></div>
                                <span class="year">June</span>
                            </div>
                            <div class="bar-seven bar-container">
                                <div class="bar" data-percentage="90%"></div>
                                <span class="year">July</span>
                            </div>
                            <div class="bar-eight bar-container">
                                <div class="bar" data-percentage="100%"></div>
                                <span class="year">Aug</span>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
