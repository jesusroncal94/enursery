@extends('layouts.aStyle', [
    'title' => 'Admin Panel - Enursery'
])


@section('header')
    @include('admin.signupmodal')
    @include('admin.totalnurseriesmodal')
    @include('admin.ordermodal')
@stop


@section('sidebar')
    @include('admin.sidebar')
@stop


@section('content')
    @include('admin.dashboard')
    @include('admin.profile')
    @include('admin.users')
    @include('admin.orders')
    @include('admin.neworders')
@stop


@section('footer')
    @include('admin.footer')
@stop


@section('scripts')
<script>
$(document).ready(function () {
    //CSRF TOKEN
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Inactive partners (start) ******************************
    // DataTable of inactive partners
    var partners_inactive_table = $('#partners_inactive').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('ajaxusers.index') }}",
            type: 'GET',
            data: {userType: 2, status_id: 2},
        },
        columns: [
            {data: 'image_upload', name: 'image_upload'},
            {data: 'name', name: 'name'},
            {data: 'contactNumber', name: 'contactNumber'},
            {data: 'email', name: 'email'},
            {data: 'address', name: 'address'},
            {data: 'role.display_name', name: 'role.display_name'},
            {data: 'status.display_name', name: 'status.display_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    // Create New Partner inactive (references to signUpModal)
    $('#signUp_btnCreateNewPartner').click(function () {
        $('#signUp_title').html("Add New Partner");
        $('#user_id').val('');
        $('#form_signUp').trigger("reset");
        var select = $('#signUp_userType');
        var select2 = $('#signUp_status_id');
        loadRoles(select, 2);
        loadStatuses(select2, 2);
    });

    // Edit a inactive partner
    $('#partners_inactive tbody').on('click', '.editUser', function () {
        var user_id = $(this).data('id');
        $.get("{{ route('ajaxusers.index') }}" +'/' + user_id +'/edit', function (data) {
            var select = $('#signUp_userType');
            var select2 = $('#signUp_status_id');
            loadRoles(select, data.role.id);
            loadStatuses(select2, data.status.id);
            $('#signUp_title').html("Edit Partner");
            $('#signUpModal').modal('show');
            $('#signUp_user_id').val(data.id);
            $('#signUp_name').val(data.name);
            $('#signUp_email').val(data.email);
            $('#signUp_userName').val(data.userName);
            $('#signUp_address').val(data.address);
            $('#signUp_contactNumber').val(data.contactNumber);
        })
    });

    // Signup (register) or update partner
    $("#form_signUp").on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            url: "{{ route('ajaxusers.store') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#form_signUp').trigger("reset");
                $('#signUpModal').modal('hide');
                partners_inactive_table.draw();
                partners_active_table.draw();
                data.success == true ? showSuccess(data) : showError(data);
            },
            error: function (data) {
                showError(data);
            }
        });
    });

    // Delete Partner
    $("#partners_inactive tbody").on('click', '.deleteUser', function () {
        var user_id = $(this).data("id");
        event.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    data: {},
                    contentType: false,
                    cache: false,
                    processData:false,
                    url: "{{ route('ajaxusers.store') }}"+'/'+user_id,
                    type: "DELETE",
                    dataType: 'json',
                    success: function (data) {
                        $('#form_signUp').trigger("reset");
                        $('#signUpModal').modal('hide');
                        partners_inactive_table.draw();
                        data.success == true ? showSuccess(data) : showError(data);
                    },
                    error: function (data) {
                        showError(data);
                    }
                });
            }
        });
    });
    // Inactive partners (end) ******************************


    // Active partners (start) ******************************
    // DataTable of inactive partners
    var partners_active_table = $('#partners_active').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('ajaxusers.index') }}",
            type: 'GET',
            data: {userType: 2, status_id: 1},
        },
        columns: [
            {data: 'image_upload', name: 'image_upload'},
            {data: 'name', name: 'name'},
            {data: 'contactNumber', name: 'contactNumber'},
            {data: 'email', name: 'email'},
            {data: 'address', name: 'address'},
            {data: 'role.display_name', name: 'role.display_name'},
            {data: 'status.display_name', name: 'status.display_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    // Create New Partner active (references to totalNurseriesModal)
    $('#totalNurseries_btnCreateNewPartner').click(function () {
        $('#totalNurseries_title').html("Add New Partner");
        $('#user_id').val('');
        $('#form_totalNurseries').trigger("reset");
        var select = $('#totalNurseries_userType');
        var select2 = $('#totalNurseries_status_id');
        loadRoles(select, 2);
        loadStatuses(select2, 1);
    });

    // Edit a active partner
    $('#partners_active tbody').on('click', '.editUser', function () {
        var user_id = $(this).data('id');
        $.get("{{ route('ajaxusers.index') }}" +'/' + user_id +'/edit', function (data) {
            var select = $('#totalNurseries_userType');
            var select2 = $('#totalNurseries_status_id');
            loadRoles(select, data.role.id);
            loadStatuses(select2, data.status.id);
            $('#totalNurseries_title').html("Edit Partner");
            $('#totalNurseriesModal').modal('show');
            $('#totalNurseries_user_id').val(data.id);
            $('#totalNurseries_name').val(data.name);
            $('#totalNurseries_email').val(data.email);
            $('#totalNurseries_userName').val(data.userName);
            $('#totalNurseries_address').val(data.address);
            $('#totalNurseries_contactNumber').val(data.contactNumber);
        })
    });

    // totalNurseries (register) or update partner
    $("#form_totalNurseries").on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            url: "{{ route('ajaxusers.store') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#form_totalNurseries').trigger("reset");
                $('#totalNurseriesModal').modal('hide');
                partners_inactive_table.draw();
                partners_active_table.draw();
                data.success == true ? showSuccess(data) : showError(data);
            },
            error: function (data) {
                showError(data);
            }
        });
    });

    // Delete Partner
    $("#partners_active tbody").on('click', '.deleteUser', function () {
        var user_id = $(this).data("id");
        event.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    data: {},
                    contentType: false,
                    cache: false,
                    processData:false,
                    url: "{{ route('ajaxusers.store') }}"+'/'+user_id,
                    type: "DELETE",
                    dataType: 'json',
                    success: function (data) {
                        $('#form_totalNurseries').trigger("reset");
                        $('#totalNurseriesModal').modal('hide');
                        partners_active_table.draw();
                        data.success == true ? showSuccess(data) : showError(data);
                    },
                    error: function (data) {
                        showError(data);
                    }
                });
            }
        });
    });
    // Active partners (end) ******************************


    // Profile (start) ******************************
    // Save/Update User (admin)
    $("#form_profile").on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            url: "{{ route('ajaxusers.store') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                data.success == true ? showSuccess(data) : showError(data);
            },
            error: function (data) {
                showError(data);
            }
        });
    });
    // Profile (end) ******************************


    // New Order (start) ******************************
    // New Orders DataTable
    var new_orders_table = $('#newOrder_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('ajaxorders.index') }}",
            type: 'GET',
            data: {
                userType: '{{ Auth::user()->userType }}',
                view: 'neworders',
            },
        },
        columns: [
            // {data: 'id', name: 'id', "render": function (data, type, row) {
            //         return 'ord000'+row.id;
            //     },
            // },
            {data: 'id', name: 'id'},
            {data: 'idFormat', name: 'idFormat'},
            {data: 'created_at', name: 'created_at'},
            {data: 'order_details', name: 'order_details', "render": function (data, type, row) {
                    return row.order_details.length;
                },
            },
            //{data: 'total', name: 'total'},
            {data: 'total', name: 'total'},
            {data: 'partner.name', name: 'partner.name', defaultContent: '-'},
            {data: 'status.display_name', name: 'status.display_name'},
            {data: 'approved_at', name: 'approved_at', defaultContent: '-'},
            {data: 'processed_at', name: 'processed_at', defaultContent: '-'},
            {data: 'delivered_at', name: 'delivered_at', defaultContent: '-'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        order: [[ 2, "desc" ]]
    });

    // View Order Details (references to ordermodal)
    $('body').on('click', '.viewOrder', function () {
        $('#orderDetail_title').text('Order Detail');
        var order_id = $(this).data('id');

        $.get("{{ route('ajaxorders.index') }}" +'/' + order_id +'/edit', function (data) {
            console.log(data);
            var personalDetails = JSON.parse(data.personalDetails);
            var orderDetails = data.order_details;
            var numItems = orderDetails.length;
            console.log(orderDetails);
            $('#orderDetail').modal('show');
            $('#orderDetail_order_id').text('Ord-'+String(data.id).padStart(6, '0'));
            $('#orderDetail_created_at').text(data.created_at);
            $('#orderDetail_customer').text(personalDetails.name);
            $('#orderDetail_email').text(personalDetails.email);
            $('#orderDetail_contactNumber').text(personalDetails.contactNumber);
            $('#orderDetail_address').text(personalDetails.address);

            $("#orderDetail tbody").html('');
            for (var i = 0; i < numItems; i++) {
                var url = '{{ URL::asset("images/products") }}';
                var url_image = url.concat('/', orderDetails[i].productImage);
                var productImage = url_image;
                $("#orderDetail tbody").append('<tr>'+
                '<td><div class="product-img"><div class="img-prdct"><img src=\"'+productImage+'\"></div></div></td>'+
                '<td>'+orderDetails[i].productName+'</td>'+
                '<td>'+orderDetails[i].pivot.productQuantity+'</td>'+
                '<td>'+orderDetails[i].pivot.productPrice+'</td>'+
                '<td align="center">'+orderDetails[i].pivot.productTotalPrice+'</td>'+
                '</tr>');
            }

            $('#orderDetail_subtotal').text(data.subtotal);
            $('#orderDetail_shipment').text(data.shipment);
            $('#orderDetail_total').text(data.total);
        });
    });

    // Approve new Order
    $('body').on('click', '.approveOrder', function () {
        var id = $(this).data('id');
        var order_id = 'Ord-'+String($(this).data('id')).padStart(6, '0');
        Swal.fire({
            title: 'Are you sure you approve the order?',
            text: "Order: "+order_id,
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, approve it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    data: {order_id: id, user_id: '{{ isset(Auth::user()->id) ? Auth::user()->id : "" }}'},
                    url: "{{ route('ajaxorders.approveOrder') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        data.success == true ? showSuccess(data) : showError(data);
                        new_orders_table.draw();
                    },
                    error: function (data) {
                        showError(data);
                    }
                });
            }
        });
    });
    // New Order (end) ******************************


    // Order (start) ******************************
    // Orders DataTable
    var orders_table = $('#totalOrders_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('ajaxorders.index') }}",
            type: 'GET',
            data: {
                userType: '{{ isset(Auth::user()->userType) ? Auth::user()->userType : "" }}',
                partner_id: '{{ isset(Auth::user()->id) ? Auth::user()->id : "" }}',
                view: 'orders',
            },
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'idFormat', name: 'idFormat'},
            {data: 'created_at', name: 'created_at'},
            {data: 'order_details', name: 'order_details', "render": function (data, type, row) {
                    var html = '';
                    for (var i = 0; i < row.order_details.length; i++) {
                        html += '<p>* '+row.order_details[i].productName+'</p>';
                    }
                    return html;
                },
            },
            {data: 'order_details', name: 'order_details', "render": function (data, type, row) {
                    var html = '';
                    for (var i = 0; i < row.order_details.length; i++) {
                        html += '<p>* '+row.order_details[i].pivot.productQuantity+'</p>';
                    }
                    return html;
                },
            },
            {data: 'total', name: 'total'},
            {data: 'partner.name', name: 'partner.name', defaultContent: '-'},
            {data: 'status.display_name', name: 'status.display_name'},
            {data: 'approved_at', name: 'approved_at', defaultContent: '-'},
            {data: 'processed_at', name: 'processed_at', defaultContent: '-'},
            {data: 'delivered_at', name: 'delivered_at', defaultContent: '-'},
        ],
        order: [[ 10, "desc" ]],
        footerCallback: function(row, data, start, end, display) {
            var api = this.api(), data;
            // Remove the formatting to get integer data for summation
            var intVal = function(i) {
                return typeof i === 'string'
                    ? Number(i)
                    : typeof i === 'number'
                        ? i
                        : 0;
            };
            // Total over this page
            pageTotal = api
                .column(5, {page: 'current'})
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            // Update footer
            $( api.column(5).footer() ).html(
                pageTotal
            );
        },
    });
    // Order (end) ******************************


    // Dashboard (start) ******************************
    $("#form_dashboard").on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            data: $(this).serialize(),
            url: "{{ route('ajaxdashboards.index') }}",
            type: "GET",
            dataType: 'json',
            success: function (data) {
                $('#subservs').hide(100);

                $partners_total = data.data.partners[0].total_nurseries;
                $partners_cant_month = data.data.partners[0].month;
                $partners_percentage_change = ($partners_total - $partners_cant_month != 0) ? (($partners_total / ($partners_total - $partners_cant_month)) * 100) - 100 : 100;
                if($partners_percentage_change > 0)
                {
                    $('#dashboard_partners').html(
                        '('+$partners_cant_month+') <br>'+
                        '<span style="font-size:15px; color:green" class="fa fa-arrow-up" id="dashboard_partners_percentage_change">'+$partners_percentage_change+'%</span>'
                    );
                } else if ($partners_percentage_change == 0)
                {
                    $('#dashboard_partners').html(
                        '('+$partners_cant_month+') <br>'+
                        '<span style="font-size:15px; color:yellow" class="fa fa-arrow-right" id="dashboard_partners_percentage_change">'+$partners_percentage_change+'%</span>'
                    );
                } else
                {
                    $('#dashboard_partners').html(
                        '('+$partners_cant_month+') <br>'+
                        '<span style="font-size:15px; color:red" class="fa fa-arrow-down" id="dashboard_partners_percentage_change">'+$partners_percentage_change+'%</span>'
                    );
                }
                $('#dashboard_partners_total').text($partners_total);

                $orders_total = data.data.orders[0].total_orders;
                $orders_cant_month = data.data.orders[0].month;
                $orders_percentage_change = ($orders_total - $orders_cant_month != 0) ? (($orders_total / ($orders_total - $orders_cant_month)) * 100) - 100 : 100;
                if($orders_percentage_change > 0)
                {
                    $('#dashboard_orders').html(
                        '('+$orders_cant_month+') <br>'+
                        '<span style="font-size:15px; color:green" class="fa fa-arrow-up" id="dashboard_orders_percentage_change">'+$orders_percentage_change+'%</span>'
                    );
                } else if ($orders_percentage_change == 0)
                {
                    $('#dashboard_orders').html(
                        '('+$orders_cant_month+') <br>'+
                        '<span style="font-size:15px; color:yellow" class="fa fa-arrow-right" id="dashboard_orders_percentage_change">'+$orders_percentage_change+'%</span>'
                    );
                } else
                {
                    $('#dashboard_orders').html(
                        '('+$orders_cant_month+') <br>'+
                        '<span style="font-size:15px; color:red" class="fa fa-arrow-down" id="dashboard_orders_percentage_change">'+$orders_percentage_change+'%</span>'
                    );
                }
                $('#dashboard_orders_total').text($orders_total);

                $revenue_total = data.data.orders[0].total_revenue;
                $revenue_cant_month = data.data.orders[0].month_revenue;
                $revenue_percentage_change = ($revenue_total - $revenue_cant_month != 0) ? (($revenue_total / ($revenue_total - $revenue_cant_month)) * 100) - 100 : 100;
                if($revenue_percentage_change > 0)
                {
                    $('#dashboard_revenue').html(
                        '('+$revenue_cant_month+') <br>'+
                        '<span style="font-size:15px; color:green" class="fa fa-arrow-up" id="dashboard_revenue_percentage_change">'+$revenue_percentage_change+'%</span>'
                    );
                } else if ($revenue_percentage_change == 0)
                {
                    $('#dashboard_revenue').html(
                        '('+$revenue_cant_month+') <br>'+
                        '<span style="font-size:15px; color:yellow" class="fa fa-arrow-right" id="dashboard_revenue_percentage_change">'+$revenue_percentage_change+'%</span>'
                    );
                } else
                {
                    $('#dashboard_revenue').html(
                        '('+$revenue_cant_month+') <br>'+
                        '<span style="font-size:15px; color:red" class="fa fa-arrow-down" id="dashboard_revenue_percentage_change">'+$revenue_percentage_change+'%</span>'
                    );
                }
                $('#dashboard_revenue_total').text($revenue_total);

                $sale_total = data.data.orders[0].total_cant_products;
                $sale_cant_month = data.data.orders[0].month_cant_products;
                $sale_percentage_change = ($sale_total - $sale_cant_month != 0) ? (($sale_total / ($sale_total - $sale_cant_month)) * 100) - 100 : 100;
                if($sale_percentage_change > 0)
                {
                    $('#dashboard_sale').html(
                        '('+$sale_cant_month+') <br>'+
                        '<span style="font-size:15px; color:green" class="fa fa-arrow-up" id="dashboard_sale_percentage_change">'+$sale_percentage_change+'%</span>'
                    );
                } else if ($sale_percentage_change == 0)
                {
                    $('#dashboard_sale').html(
                        '('+$sale_cant_month+') <br>'+
                        '<span style="font-size:15px; color:yellow" class="fa fa-arrow-right" id="dashboard_sale_percentage_change">'+$sale_percentage_change+'%</span>'
                    );
                } else
                {
                    $('#dashboard_sale').html(
                        '('+$sale_cant_month+') <br>'+
                        '<span style="font-size:15px; color:red" class="fa fa-arrow-down" id="dashboard_sale_percentage_change">'+$sale_percentage_change+'%</span>'
                    );
                }
                $('#dashboard_sale_total').text($sale_total);

                $('#subservs').show(1000);
            },
            error: function (data) {
                showError(data);
            }
        });
    });

    var year = '{{ date("Y") }}';
    var month = '{{ date("m") }}'.replace(/^0+/, '');
    $("#dashboard_year option[value="+year+"]").attr('selected', 'selected');
    $("#dashboard_month option[value="+month+"]").attr('selected', 'selected');
    $("#form_dashboard").trigger('submit');
    // Dashboard (end) ******************************
});


// Load Roles
function loadRoles(select, role_id = 0)
{
    select.empty().append("<option value=0>.:Select Role</option>");

    $.ajax({
        data: {},
        url: "{{ route('ajaxroles.index') }}",
        type: "GET",
        dataType: 'json',
        success: function (data) {
          $.each(data, function(index, value) {
              if (value.id == role_id)
              {
                  select.append('<option selected value=' + value.id + '>' + value.display_name + '</option>');
              } else
              {
                  select.append('<option value=' + value.id + '>' + value.display_name + '</option>');
              }
          });
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}


// Load Statuses
function loadStatuses(select, status_id = 0)
{
    select.empty().append("<option value=0>.:Select Status</option>");

    $.ajax({
        data: {},
        url: "{{ route('ajaxstatuses.index') }}",
        type: "GET",
        dataType: 'json',
        success: function (data) {
          $.each(data, function(index, value) {
              if (value.id == status_id)
              {
                  select.append('<option selected value=' + value.id + '>' + value.display_name + '</option>');
              } else
              {
                  select.append('<option value=' + value.id + '>' + value.display_name + '</option>');
              }
          });
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}
</script>
@stop
