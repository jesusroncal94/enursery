<div class="tab-pane fade" id="newOrder">
    <div class="col-xs-offset-1 col-xs-10">
        <div class="row">
            <div>
                <div class="registration-info">
                    <h1>
                        New Orders
                    </h1>
                    <p>
                        New Orders on {{ date("F j, Y, g:i a") }}
                    </p>
                </div>
                <div class="table-background">
                    <table id="newOrder_table" class="table text-center">
                        <thead>
                            <tr>
                                <th class="text-center" scope="col">Id</th>
                                <th class="text-center" scope="col">Order Id</th>
                                <th class="text-center" scope="col">Date</th>
                                <th class="text-center" scope="col">Total Items</th>
                                <th class="text-center" scope="col">Amount(Rs.)</th>
                                <th class="text-center" scope="col">Partner</th>
                                <th class="text-center" scope="col">Status</th>
                                <th class="text-center" scope="col">Approved at</th>
                                <th class="text-center" scope="col">Processed at</th>
                                <th class="text-center" scope="col">Delivered at</th>
                                <th class="text-center" scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- <tr>
                                <td scope="row">ord102</td>
                                <td>10 june 2020 </td>
                                <td>10</td>
                                <td>25485</td>
                                <td>Panding</td>
                                <td>
                                    <a data-toggle="modal" data-target="#orderDetail" class="btn btn-sm"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr> -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
