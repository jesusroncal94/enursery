<aside class="sidebar">
    <ul class="no-list sb_top_lv">
        <li class="userLogo" style="color:rgb(18, 49, 50);background-color:rgb(168, 101, 80);">
            <i class="fa fa-user"></i><span><b>{{ Auth::user()->name }}</b></span>
        </li>

        <li class="active">
            <a data-toggle="tab" class="outer-a" href="#dashboard">
                <i class="fa fa-dashboard"></i><span>Dashboard</span>
            </a>
        </li>
        <li>
            <a data-toggle="tab" class="outer-a" href="#profile">
                <i class="fa fa-user-circle-o"></i><span>Profile</span>
            </a>
        </li>
        <li>
            <i class="fa fa-handshake-o"></i><span class="outer-a">Partner Nurseries</span>
            <ul class="no-list sb_dropdown clearfix">
                <li>
                    <a data-toggle="tab" style="" href="#totalNurseries">
                        Total Partner <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nurseries
                    </a>
                </li>
                <li>
                    <a id="users" data-toggle="tab" class="inner-a" href="#registration">
                        Registration
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <i class="fa fa-shopping-cart"></i><span class="outer-a">Orders</span>
            <ul class="no-list sb_dropdown clearfix">
                <li>
                    <a data-toggle="tab" style="" href="#totalOrders">
                        Total Orders
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" style="" href="#newOrder">
                        New Order(s)
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('ajaxusers.logout') }}" data-toggle="" class="outer-a">
                <i class="fa fa-sign-out"></i><span>Logout</span>
            </a>
        </li>
    </ul>
</aside>
