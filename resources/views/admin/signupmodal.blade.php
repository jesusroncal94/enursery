<div class="modal fade" id="signUpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="text-center order-details" id="signUp_title"></h4>
            </div>
            <form id="form_signUp" name="form_signUp" class="form-inline" >
            <div class="modal-body">
                <div class="signUpModal">
                        <div class="has-feedback form-inline">
                            <input type="hidden" name="user_id" id="signUp_user_id">
                            <div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="fa fa-edit"></span>
                                    </span>
                                    <input id="signUp_name" type="text" class="form-control" placeholder="Name*" name="name" required />
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="fa fa-mobile-phone"></span>
                                    </span>
                                    <input id="signUp_contactNumber" type="tel" class="form-control" placeholder="Contact No*" name="contactNumber" required />
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="fa fa-envelope-o"></span>
                                    </span>
                                    <input id="signUp_email" type="email" class="form-control" placeholder="Email*" name="email" required />
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="far fa-address-book"></span>
                                    </span>
                                    <input id="signUp_address" type="text" class="form-control" placeholder="Address*" name="address" required />
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="fas fa-user"></span>
                                    </span>
                                    <input id="signUp_userName" type="text" class="form-control" placeholder="Username*" name="userName" required />
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="fas fa-lock"></span>
                                    </span>
                                    <input id="signUp_password" type="password" class="form-control" placeholder="Password*" name="password" />
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fas fa-users"></span> </span>
                                    <select id="signUp_userType" class="form-control" name="userType">
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-warning"></span> </span>
                                    <select id="signUp_status_id" class="form-control" name="status_id">
                                    </select>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button id="signUp_btnSavePartner" type="submit" class="btn btn-sm update-profile mr-auto">Save Partner</button>
                <button type="button" data-dismiss="modal" class="btn btn-sm update-profile">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>
