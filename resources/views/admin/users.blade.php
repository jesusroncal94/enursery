<div class="tab-pane fade" id="registration">
    <div class="col-xs-offset-1 col-xs-10">
        <div class="row">
            <div>
                <div class="registration-info">
                    <h1>
                        Nursery Stats
                    </h1>
                    <p>
                        New Nurseries on {{ date("F j, Y, g:i a") }}
                    </p>
                </div>

                <div class="table-background">
                    <table id="partners_inactive" class="table text-center data-table" width="100%">
                        <thead>
                            <tr>
                                <th colspan="8" style="cursor:pointer;">
                                    <a id="signUp_btnCreateNewPartner" data-toggle="modal" data-target="#signUpModal">
                                        <span style="color:rgb(18, 49, 50)" class="pull-left">Add New</span>
                                        <i style="color:rgb(18, 49, 50)" class="fa fa-plus pull-right"></i>
                                    </a>
                                </th>
                            </tr>
                            <tr>
                                <th class="text-center">Image</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Contact</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Address</th>
                                <th class="text-center">Role</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="tab-pane fade" id="totalNurseries">
    <div class="col-xs-offset-1 col-xs-10">
        <div class="row">
            <div>
                <div class="registration-info">
                    <h1>
                        Total Nurseries
                    </h1>
                    <p>
                        Total Nurseries on {{ date("F j, Y, g:i a") }}
                    </p>
                </div>
                <div class="table-background">
                    <table id="partners_active" class="table text-center" width="100%">
                        <thead>
                            <tr>
                                <th colspan="8" style="cursor:pointer;">
                                    <a id="totalNurseries_btnCreateNewPartner" data-toggle="modal" data-target="#totalNurseriesModal">
                                        <span style="color:rgb(18, 49, 50)" class="pull-left">Add New</span>
                                        <i style="color:rgb(18, 49, 50)" class="fa fa-plus pull-right"></i>
                                    </a>
                                </th>
                            </tr>
                            <tr>
                                <th class="text-center">Image</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Contact</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Address</th>
                                <th class="text-center">Role</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
