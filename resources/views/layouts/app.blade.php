<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8" />
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href='https://fonts.googleapis.com/css?family=Kalam' rel='stylesheet'>
		<link href="assets/bstrapfiles/bootstrap-theme.css" rel="stylesheet" />
		<link href="assets/bstrapfiles/bootstrap.css" rel="stylesheet" />
		<link href="assets/bstrapfiles/main.css" rel="stylesheet" />
		<link href="assets/appfiles/eNursery.css" rel="stylesheet" />
		<link href="assets/appfiles/load-more-button.css" rel="stylesheet" />
		<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

		<style>
		.dataTables_paginate a {
				padding: 6px 9px !important;
				background: #123132 !important;
				border-color: #ddd !important;
		}

		table,
		thead,
		th {
		  	color: black !important;
		}
		</style>

		<title>{{ $title }}</title>
</head>
<body>
		@include('layouts.navbar')

		<!--Header-->
		<header>
				@yield('header')
		</header>

		<!--Main-->
		<main>
				@yield('content')
		</main>

		@include('layouts.footer')

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="assets/jsfiles/bootstrap.js"></script>
		<script src="assets/appfiles/eNursery.js"></script>
		<!-- <script src="assets/jsfiles/jquery-1.9.1.js"></script> -->
		<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.15.1/dist/sweetalert2.all.min.js"></script>

		<script>
		// Show a sweetalert2 in success case
		function showSuccess(data, reloadPage = false)
		{
				console.log('Success:', data);
				var message = '';
				$.each(data.data, function(index, value) {
						message += index + ': ' + value + ', ';
				});
				Swal.fire({
						position: 'center',
						icon: 'success',
						title: data.message,
						text: message.substring(0, message.length - 2),
						showConfirmButton: true,
						timer: 3000,
				}).then((result) => {
						(reloadPage == true) ? window.location.replace("/") : '';
						if (result.value) {
								(reloadPage == true) ? window.location.replace("/") : '';
						}
				});
		}

		// Show a sweetalert2 success when a product is added
		function showSuccessAddProduct(productName)
		{
				Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Product added to cart successfully',
						text: 'Product: ' + productName,
						showConfirmButton: true,
						timer: 3000,
				});
		}

		// Show a sweetalert2 in error case
		function showError(data)
		{
				console.log('Error:', data);
				var message = '';
				$.each(data.responseJSON.data, function(index, value) {
						message += index + ': ' + value + ', ';
				});
				Swal.fire({
						position: 'center',
						icon: 'error',
						title: data.responseJSON.message,
						text: message.substring(0, message.length - 2),
						showConfirmButton: true,
				});
		}


		$(document).ready(function () {
				//CSRF TOKEN
				$.ajaxSetup({
						headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
				});


				// Cart (start) *********************************************
				loadCart();
				// Cart Modals Next & Prev
				$("div[id^='cartModal']").each(function () {
						var currentModal = $(this);
								//click next
								currentModal.find('.btn-next').click(function () {
										currentModal.modal('hide');
										currentModal.closest("div[id^='cartModal']").nextAll("div[id^='cartModal']").first().modal('show');
								});

						//click prev
								currentModal.find('.btn-prev').click(function () {
								currentModal.modal('hide');
								currentModal.closest("div[id^='cartModal']").prevAll("div[id^='cartModal']").first().modal('show');
						});

						currentModal.find('.btn-change').click(function () {
								currentModal.modal('hide');
								$('#cartModal1').modal('show');
						});
				});

				$("#productQuantity").change(function(){
						var total = Number($(this).val())*Number($('#productPrice').text());
						$('#productTotalPrice').text(total);
				});

				$(document).on('click', '.cart-icon', function() {
						if ("{{ Auth::check() }}" != "") {
								$('#cartModal1').modal('show');
						} else {
								Swal.fire({
										title: 'User no logged in',
										text: "Please, to continue sign in or sign up. Thanks!",
										icon: 'warning',
										showCancelButton: true,
										confirmButtonColor: '#3085d6',
										cancelButtonColor: '#d33',
										confirmButtonText: 'Continue'
								}).then((result) => {
										if (result.value) {
												$('#loginModal').modal('show');
										}
								});
						}
				});

				$('#cartModal2_btnSaveAll').click(function() {
						$('#nameModal3').html('&nbsp;'+$('#cartModal2_name').val());
						$('#addressModal3').html('&nbsp;'+$('#cartModal2_address').val());
						$('#telModal3').html('&nbsp;'+$('#cartModal2_tel').val());
						$('#emailModal3').html('&nbsp;'+$('#cartModal2_email').val());

						if ("{{ Auth::check() }}" != "") {
								$.get("{{ isset(Auth::user()->id) ? route('ajaxusers.loadCart', Auth::user()->id) : '' }}", function (data) {
										cart = JSON.parse(data);
										var numItems = JSON.parse(data).length;
										$("#mycartPreview tbody").html("");

										for (var i = 0; i < numItems; i++) {
												var url = '{{ URL::asset("images/products") }}';
												var url_image = url.concat('/', cart[i].productImage);
												var productImage = url_image;
												$("#mycartPreview tbody").append('<tr>'+
												'<td><div class="product-img"><div class="img-prdct"><img src=\"'+productImage+'\"></div></div></td>'+
												'<td>'+cart[i].productName+'</td>'+
												'<td>'+cart[i].productQuantity+'</td>'+
												'<td>'+cart[i].productPrice+'</td>'+
												'<td align="center">'+cart[i].productTotalPrice+'</td>'+
												'</tr>');
										}
								});
						}
				});

				$('#cartModal3_btnChangeCart').click(function() {
						$('#cartModal3').modal('hide');
						$('#cartModal1').modal('show');
				});

				$('#cartModal3_btnCreateOrder').click(function() {
						// alert('Orden creada');
						// Order
						var json_order = [];
						var json_personalDetails = [];
						json_personalDetails.push(
								{name:  $('#cartModal2_name').val(), contactNumber:  $('#cartModal2_tel').val(), address:  $('#cartModal2_address').val(), email:  $('#cartModal2_email').val()}
						);
						json_order.push(
						    {user_id: "{{ isset(Auth::user()->id) ? Auth::user()->id : '' }}", subtotal: $('#total').text(), shipment: $('#shipment').text(), total: $('#Payable').text(), personalDetails: json_personalDetails, status_id: 3}
						);

						// Order Details
						var json = "";
						var json_total = "";
						$("#mycart tbody tr").each(function ()
						{
								json = "";
								$(this).find("td").each(function (index, content)
								{
										if(index == 0){
												json = json + ',"product_id": "'+ $(this).text() + '"';
										} else if (index == 3) {
												json = json + ',"productQuantity": "'+ $(this).find('.qty').val() + '"';
										} else if (index == 4) {
												json = json + ',"productPrice": "'+ $(this).find('input').val() + '"';
										} else if (index == 5) {
												json = json + ',"productTotalPrice": "'+ $(this).find('span').text() + '"';
										}
								});
								obj = JSON.parse('{' + json.substr(1) + '}');
								json_total = json_total + ',' + JSON.stringify(obj);
						});
						var array_json = JSON.parse('[' + json_total.substr(1) + ']');

						$.ajax({
		            data: {order: json_order, orderDetails: array_json},
		            url: "{{ route('ajaxorders.store') }}",
		            type: "POST",
		            dataType: 'json',
		            success: function (data) {
		                data.success == true ? showSuccess(data, true) : showError(data);
		            },
		            error: function (data) {
		                showError(data);
		            }
		        });
						orders_table.draw();
				});
				// Cart (end) ***********************************************



				// SignUp (start) ******************************
				// Create New User (references to signUpModal)
				$('#createNewUser').click(function () {
						$('#signUp_user_id').val('');
						$('#form_signUp').trigger("reset");
				});

				// Signup (register)
				$('#signUp_btnSignUp').click(function (event) {
						event.preventDefault();
						$(this).html('Creating..');
						$.ajax({
								data: $('#form_signUp').serialize(),
								url: "{{ route('ajaxusers.store') }}",
								type: "POST",
								dataType: 'json',
								success: function (data) {
										$('#form_signUp').trigger("reset");
										$('#signUpModal').modal('hide');
										data.success == true ? showSuccess(data) : showError(data);
										$('#loginModal').modal('show');
								},
								error: function (data) {
										$('#signUpModal').modal('hide');
										showError(data);
								}
						});
				});
				// SignUp (end) ******************************


				// Login or SignIn (start) ******************************
				//btn_loginUser
				$('#btn_loginUser').click(function () {
						$('#form_loginUser').trigger("reset");
				});

				//login_btnSignUp
				$('#login_btnSignUp').click(function () {
						$('#signUp_user_id').val('');
						$('#form_signUp').trigger("reset");
				});

				// Login or Signin
				$('#login_btnSignIn').click(function (event) {
						event.preventDefault();
						$(this).html('Login..');
						$.ajax({
								data: $('#form_loginUser').serialize(),
								url: "{{ route('ajaxusers.login') }}",
								type: "POST",
								dataType: 'json',
								success: function (data) {
										$('#form_loginUser').trigger("reset");
										$('#loginModal').modal('hide');
										data.success == true ? showSuccess(data, true) : showError(data);
								},
								error: function (data) {
										$('#loginModal').modal('hide');
										showError(data);
								}
						});
				});
				// Login or SignIn (end) ******************************


				// Order (start) ******************************
		    // Orders DataTable
				if('{{ isset(Auth::user()->userType) ? Auth::user()->userType : "" }}' == 3)
				{
				    var orders_table = $('#orders_table').DataTable({
				        processing: true,
				        serverSide: true,
				        ajax: {
				            url: "{{ route('ajaxorders.index') }}",
				            type: 'GET',
				            data: {
				                userType: '{{ isset(Auth::user()->userType) ? Auth::user()->userType : "" }}',
				                user_id: '{{ isset(Auth::user()->id) ? Auth::user()->id : "" }}',
				                view: 'orders',
				            },
				        },
				        columns: [
				            {data: 'idFormat', name: 'idFormat'},
				            {data: 'created_at', name: 'created_at'},
				            {data: 'order_details', name: 'order_details', "render": function (data, type, row) {
				                    var html = '';
				                    for (var i = 0; i < row.order_details.length; i++) {
				                        html += '<p>* '+row.order_details[i].productName+'</p>';
				                    }
				                    return html;
				                },
				            },
				            {data: 'order_details', name: 'order_details', "render": function (data, type, row) {
				                    var html = '';
				                    for (var i = 0; i < row.order_details.length; i++) {
				                        html += '<p>* '+row.order_details[i].pivot.productQuantity+'</p>';
				                    }
				                    return html;
				                },
				            },
				            {data: 'total', name: 'total'},
				            // {data: 'partner.name', name: 'partner.name', defaultContent: '-'},
				            {data: 'status.display_name', name: 'status.display_name'},
				            // {data: 'approved_at', name: 'approved_at', defaultContent: '-'},
				            // {data: 'processed_at', name: 'processed_at', defaultContent: '-'},
				            // {data: 'delivered_at', name: 'delivered_at', defaultContent: '-'},
				        ],
				        order: [[ 0, "desc" ]],
								ordering: false,
								searching: false,
								info: false,
								drawCallback: function() {
										$('#orders_table_length').hide();
								},
				        footerCallback: function(row, data, start, end, display) {
				            var api = this.api(), data;
				            // Remove the formatting to get integer data for summation
				            var intVal = function(i) {
				                return typeof i === 'string'
				                    ? Number(i)
				                    : typeof i === 'number'
				                        ? i
				                        : 0;
				            };
				            // Total over this page
				            pageTotal = api
				                .column(4, {page: 'current'})
				                .data()
				                .reduce(function(a, b) {
				                    return intVal(a) + intVal(b);
				                }, 0);
				            // Update footer
				            $( api.column(4).footer() ).html(
				                pageTotal
				            );
				        },
				    });
				}
		    // Order (end) ******************************
		});


		function addToCart(product_id) {
				if ("{{ Auth::check() }}" != "") {
						$.get("{{ route('ajaxproducts.index') }}" +'/' + product_id +'/edit', function (data) {
								var url = '{{ URL::asset("images/products") }}';
								var url_image = url.concat('/', data.productImage);
								var productImage = url_image;
								var productName = data.productName;
								var productQuantity = 1;
								var productPrice = data.productPrice;
								var productTotalPrice = data.productPrice;

								var add_product = true;
								$("#mycart tbody tr").each(function ()
								{
										$(this).find("td").each(function (index, content)
										{
												if(index == 0 && $(this).text() == product_id)
												{
														fila = $(this).closest("tr");
														fila.find('td:eq(0)').css({'display': 'none'});
														fila.find('td:eq(0)').text(product_id);
														fila.find('td:eq(1)').html('<div class="product-img"><div class="img-prdct"><img src=\"'+productImage+'\"></div></div>');
														fila.find('td:eq(2)').html('<p>'+productName+'</p>');
														var new_productQuantity = Number(productQuantity) + Number(fila.find('td:eq(3)').find(".qty").val());
														fila.find('td:eq(3)').html('<div class="button-container"><button onclick="incrementQty.call(this);" class="cart-qty-plus" type="button" value="+">+</button><input type="text" name="qty" min="1" class="qty form-control" value=\"'+new_productQuantity+'\" /><button onclick="decrementQty.call(this);" class="cart-qty-minus" type="button" value="-">-</button></div>');
														fila.find('td:eq(4)').html('<input type="text" value=\"'+productPrice+'\" class="price form-control" disabled>');
														fila.find('td:eq(5)').attr('align', 'right');
														fila.find('td:eq(5)').html('$ <span id="amount" class="amount">'+productTotalPrice+'</span>');
														fila.find('td:eq(6)').html('<div class="button-container"><button onclick="deleteItemFromCart.call(this);" class="btn btn-danger btn-sm" type="button" value="X">X</button></div>');
														add_product = false;
														showSuccessAddProduct(productName);
														return false;
												}
										});
								});

								if(add_product == true)
								{
										$("#mycart tbody").append('<tr>'+
										'<td style="display:none;">'+product_id+'</td>'+
										'<td><div class="product-img"><div class="img-prdct"><img src=\"'+productImage+'\"></div></div></td>'+
										'<td><p>'+productName+'</p></td>'+
										'<td><div class="button-container"><button onclick="incrementQty.call(this);" class="cart-qty-plus" type="button" value="+">+</button><input type="text" name="qty" min="1" class="qty form-control" value=\"'+productQuantity+'\" /><button onclick="decrementQty.call(this);" class="cart-qty-minus" type="button" value="-">-</button></div></td>'+
										'<td><input type="text" value=\"'+productPrice+'\" class="price form-control" disabled></td>'+
										'<td align="right">$ <span id="amount" class="amount">'+productTotalPrice+'</span></td>'+
										'<td><div class="button-container"><button onclick="deleteItemFromCart.call(this);" class="btn btn-danger btn-sm" type="button" value="X">X</button></div></td>'+
										'</tr>');
										incrementNotification();
										showSuccessAddProduct(productName);
								}

								update_amounts();
								addCartToDatabase();
						});
				} else {
						Swal.fire({
								title: 'User no logged in',
								text: "Please, to continue sign in or sign up. Thanks!",
								icon: 'warning',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'Continue'
						}).then((result) => {
								if (result.value) {
										$('#loginModal').modal('show');
								}
						});
				}
		}


		function addToCartFromModal(product_id) {
				if ("{{ Auth::check() }}" != "") {
						var productImage = $('#productImage').attr('src');
						var productName = $('#productName').text();
						var productQuantity = $('#productQuantity').val();
						var productPrice = $('#productPrice').text();
						var productTotalPrice = $('#productTotalPrice').text();

						var add_product = true;
						$("#mycart tbody tr").each(function ()
						{
								$(this).find("td").each(function (index, content)
								{
										if(index == 0 && $(this).text() == product_id)
										{
												fila = $(this).closest("tr");
												fila.find('td:eq(0)').css({'display': 'none'});
												fila.find('td:eq(0)').text(product_id);
												fila.find('td:eq(1)').html('<div class="product-img"><div class="img-prdct"><img src=\"'+productImage+'\"></div></div>');
												fila.find('td:eq(2)').html('<p>'+productName+'</p>');
												var new_productQuantity = Number(productQuantity) + Number(fila.find('td:eq(3)').find(".qty").val());
												fila.find('td:eq(3)').html('<div class="button-container"><button onclick="incrementQty.call(this);" class="cart-qty-plus" type="button" value="+">+</button><input type="text" name="qty" min="1" class="qty form-control" value=\"'+new_productQuantity+'\" /><button onclick="decrementQty.call(this);" class="cart-qty-minus" type="button" value="-">-</button></div>');
												fila.find('td:eq(4)').html('<input type="text" value=\"'+productPrice+'\" class="price form-control" disabled>');
												fila.find('td:eq(5)').attr('align', 'right');
												fila.find('td:eq(5)').html('$ <span id="amount" class="amount">'+productTotalPrice+'</span>');
												fila.find('td:eq(6)').html('<div class="button-container"><button onclick="deleteItemFromCart.call(this);" class="btn btn-danger btn-sm" type="button" value="X">X</button></div>');
												add_product = false;
												showSuccessAddProduct(productName);
												$('#details').hide();
												return false;
										}
								});
						});

						if(add_product == true)
						{
								$("#mycart tbody").append('<tr>'+
								'<td style="display:none;">'+product_id+'</td>'+
								'<td><div class="product-img"><div class="img-prdct"><img src=\"'+productImage+'\"></div></div></td>'+
								'<td><p>'+productName+'</p></td>'+
								'<td><div class="button-container"><button onclick="incrementQty.call(this);" class="cart-qty-plus" type="button" value="+">+</button><input type="text" name="qty" min="1" class="qty form-control" value=\"'+productQuantity+'\" /><button onclick="decrementQty.call(this);" class="cart-qty-minus" type="button" value="-">-</button></div></td>'+
								'<td><input type="text" value=\"'+productPrice+'\" class="price form-control" disabled></td>'+
								'<td align="right">$ <span id="amount" class="amount">'+productTotalPrice+'</span></td>'+
								'<td><div class="button-container"><button onclick="deleteItemFromCart.call(this);" class="btn btn-danger btn-sm" type="button" value="X">X</button></div></td>'+
								'</tr>');
								incrementNotification();
								showSuccessAddProduct(productName);
								$('#details').hide();
						}

						update_amounts();
						addCartToDatabase();
				} else {
						$('#details').hide();
						Swal.fire({
								title: 'User no logged in',
								text: "Please, to continue sign in or sign up. Thanks!",
								icon: 'warning',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'Continue'
						}).then((result) => {
								if (result.value) {
										$('#loginModal').modal('show');
								}
						});
				}
		}


		function showProductDetails(product_id) {
				$.get("{{ route('ajaxproducts.index') }}" +'/' + product_id +'/edit', function (data) {
						$('#details').show();
						$('#productName').text(data.productName);
						$('#productDescription').text(data.productDescription);
						$("#productQuantity").val(1);
						$('#productPrice').text(data.productPrice);
						$('#productTotalPrice').text(data.productPrice);
						var url = '{{ URL::asset("images/products") }}';
						var url_image = url.concat('/', data.productImage);
						$("#productImage").attr("src", url_image);
						$("#details_btnAddToCart").attr('onclick', 'addToCartFromModal('+data.id+');');
				});
		}


		//----------------for quantity-increment-or-decrement-by-button-------
		//increment
		function incrementQty() {
				//var plusBtn = $(".cart-qty-plus");
				var $n = $(this)
						.parent(".button-container")
						.find(".qty");
				$n.val(Number($n.val()) + 1);
				update_amounts();
				addCartToDatabase();
		}
		//decrement
		function decrementQty() {
				//var minusBtn = $(".cart-qty-minus");
				var $n = $(this)
						.parent(".button-container")
						.find(".qty");
				var QtyVal = Number($n.val());
				if (QtyVal > 1) {
						$n.val(QtyVal - 1);
				}
				update_amounts();
				addCartToDatabase();
		}
		//increment cart value Notification
		function incrementNotification() {
				var b = parseInt($('.badge2').text());
				if (b == "") {
						b = 0;
				}
				$('.badge').text(b+1);
				$('.badge2').text(b+1);
		}
		//decrement cart value Notification
		function decrementNotification() {
				var b = parseInt($('.badge2').text());
				if (b == "") {
						b = 0;
				}
				$('.badge').text(b-1);
				$('.badge2').text(b-1);
		}
		//set cart value Notification
		function setNotification(value) {
				var b = parseInt($('.badge2').text());
				$('.badge').text(value);
				$('.badge2').text(value);
		}

		function update_amounts() {
		    var sum = 0.0;
		    $('#mycart > tbody  > tr').each(function () {
		        var qty = $(this).find('.qty').val();
		        var price = $(this).find('.price').val();
		        var amount = (qty * price)
		        sum += amount;
		        $(this).find('.amount').text('' + amount);
		    });
		    $('.total').text(sum);
				//var shipment = sum*0.18;
				var shipment = 25;
				$('.shipment').text(shipment);
				$('.Payable').text(sum+shipment);
		}


		function deleteItemFromCart() {
				$(this).closest('tr').remove();
				update_amounts();
				decrementNotification();
				addCartToDatabase();
		};


		function addCartToDatabase() {
				var json = "";
				var json_total = "";
				$("#mycart tbody tr").each(function ()
				{
						json = "";
						$(this).find("td").each(function (index, content)
						{
								if(index == 0){
										json = json + ',"id": "'+ $(this).text() + '"';
								} else if (index == 1) {
										json = json + ',"productImage": "'+ $(this).find('img').attr('src').split('/')[5] + '"';
								} else if (index == 2) {
										json = json + ',"productName": "'+ $(this).find('p').text() + '"';
								} else if (index == 3) {
										json = json + ',"productQuantity": "'+ $(this).find('.qty').val() + '"';
								} else if (index == 4) {
										json = json + ',"productPrice": "'+ $(this).find('input').val() + '"';
								} else if (index == 5) {
										json = json + ',"productTotalPrice": "'+ $(this).find('span').text() + '"';
								}
						});
						obj = JSON.parse('{' + json.substr(1) + '}');
						json_total = json_total + ',' + JSON.stringify(obj);
				});
				var array_json = JSON.parse('[' + json_total.substr(1) + ']');

				$.ajax({
						data: {user_id: '{{ isset(Auth::user()->id) ? Auth::user()->id : "" }}', cart: array_json},
						url: "{{ route('ajaxusers.updateCart') }}",
						type: "POST",
						dataType: 'json',
						success: function (data) {
								console.log(JSON.stringify(data));
								// $('#form_signUp').trigger("reset");
								// $('#signUpModal').modal('hide');
								// partners_inactive_table.draw();
								// partners_active_table.draw();
								// data.success == true ? showSuccess(data) : showError(data);
						},
						error: function (data) {
								console.log(JSON.stringify(data));
						}
				});
		}


		function loadCart() {
				if('{{ Auth::check() }}')
				{
						$.get("{{ isset(Auth::user()->id) ? route('ajaxusers.loadCart', Auth::user()->id) : '' }}", function (data) {
								if(data != null || data != undefined)
								{
										cart = JSON.parse(data);
										var numItems = JSON.parse(data).length;

										setNotification(numItems);
										for (var i = 0; i < numItems; i++) {
												var url = '{{ URL::asset("images/products") }}';
												var url_image = url.concat('/', cart[i].productImage);
												var productImage = url_image;
												$("#mycart tbody").append('<tr>'+
												'<td style="display:none;">'+cart[i].id+'</td>'+
												'<td><div class="product-img"><div class="img-prdct"><img src=\"'+productImage+'\"></div></div></td>'+
												'<td><p>'+cart[i].productName+'</p></td>'+
												'<td><div class="button-container"><button onclick="incrementQty.call(this);" class="cart-qty-plus" type="button" value="+">+</button><input type="text" name="qty" min="1" class="qty form-control" value=\"'+cart[i].productQuantity+'\" /><button onclick="decrementQty.call(this);" class="cart-qty-minus" type="button" value="-">-</button></div></td>'+
												'<td><input type="text" value=\"'+cart[i].productPrice+'\" class="price form-control" disabled></td>'+
												'<td align="right">$ <span id="amount" class="amount">'+cart[i].productTotalPrice+'</span></td>'+
												'<td><div class="button-container"><button onclick="deleteItemFromCart.call(this);" class="btn btn-danger btn-sm" type="button" value="X">X</button></div></td>'+
												'</tr>');
										}
										update_amounts();
								}
						});
			  }
		}
		</script>

		@yield('scripts')
</body>
</html>
