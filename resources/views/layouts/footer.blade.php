 <!--Footer-->
<footer class="container-fluid">
    <div id="footer" class="col-xs-offset-1 col-xs-10">
        <!--Quick Links-->
        <div class="col-md-4 col-xs-12">
            <div style="width:100%; margin:auto; left:50%">
                <h3> Quick Links</h3>
                <hr style="margin-top:-3px" class="hidden-xs" />
                <a href="#"><span class="glyphicon glyphicon-chevron-right"></span> HOME</a><br />
                <a href="#"><span class="glyphicon glyphicon-chevron-right"></span> SHOP</a><br />
                <a href="#"><span class="glyphicon glyphicon-chevron-right"></span> About</a><br />
                <a href="#"><span class="glyphicon glyphicon-chevron-right"></span> FAQ</a><br />
                <a href="#"><span class="glyphicon glyphicon-chevron-right"></span> CONTACT</a><br />
            </div>
        </div>
        <hr class="hidden visible-xs" style="width:60%;margin-left:20px" />
        <!--Contact-->
        <div class="col-md-4 col-xs-12">
            <div style="width:100%; margin:auto; left:50%">
                <h3> Contact</h3>
                <hr style="margin-top:-3px" class="hidden-xs" />
                <a href="#"><span class="glyphicon glyphicon-map-marker"> </span> khalid bin waleed hall, Lahore</a><br />
                <a href="#"><span class="glyphicon glyphicon-phone-alt"> </span> 00-000-0000000</a><br />
                <a href="#"><span class="glyphicon glyphicon-envelope"> </span> eNursery@gmail.com</a><br />
                <br />
            </div>
        </div>
        <hr class="hidden visible-xs" style=" width:60%;margin-left:20px" />
        <!--Subscribe-->
        <div class="col-md-4 col-xs-12">
            <div style="width:100%; margin:auto; left:50%">
                <h3>Subscribe Now</h3>
                <hr style="margin-top:-3px" class="hidden-xs" />
                <div class="has-feedback form-inline">
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span> </span>
                        <input id="suscribe_email" type="text" class="form-control" placeholder="Enter Your Email Here*" />
                    </div>
                    <button href="#" class="btn btn-sm btn-danger pull-right">SUBSCRIBE</button>
                </div>
            </div>
        </div>
        <hr class="hidden visible-xs" style=" width:60%;margin-left:20px" />
    </div>
    <!--Social Media-->
    <div class="social-media container-fluid pull-right" style="margin-right:50px;">
        <a href="#" class="fa fa-facebook"></a>
        <a href="#" class="fa fa-twitter"></a>
        <a href="#" class="fa fa-google"></a>
        <a href="#" class="fa fa-linkedin"></a>
        <a href="#" class="fa fa-youtube"></a>
        <a href="#" class="fa fa-instagram"></a>
    </div>
</footer>
