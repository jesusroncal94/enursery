<header>
    <!--Navbar-->
    <nav class="nav navbar navbar-fixed-top">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar navbar-inverse"></span>
                <span class="icon-bar navbar-inverse"></span>
                <span class="icon-bar navbar-inverse"></span>
            </button>
            <div class="">
                <a class="navbar-brand " href="#">
                    <img class="tag" src="images/LogoMakr_4dBNLH1.png" />
                </a>
                <div class="notification">
                    <a class="btn cart-icon pull-right"><span style="margin-top:5px;" class="glyphicon glyphicon-shopping-cart"><span class="badge">0</span></span></a>
                </div>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="#home">HOME</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SHOP<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class=""><a href="viewAllProducts.html">Plants</a></li>
                        <li class=""><a href="viewAllProducts.html">Trees</a></li>
                    </ul>
                </li>
                <li><a href="#about"> ABOUT</a></li>
                <li><a href="#faq"> FAQ </a></li>
                <li><a href="#contact"> CONTACT</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::check())
                <li ><a data-toggle="modal" data-target="#" class="btn"><span class="glyphicon glyphicon-user"> </span> {{ Auth::user()->userName }}</a></li>
                    @if(Auth::user()->userType == 1)
                        <li ><a href="{{ route('admin') }}" target="_blank" class="btn"><span class="glyphicon glyphicon-eye-open"> </span> Admin Panel</a></li>
                    @elseif(Auth::user()->userType == 2)
                        <li ><a href="{{ route('partner') }}" target="_blank" class="btn"><span class="glyphicon glyphicon-eye-open"> </span> Partner Panel</a></li>
                    @elseif(Auth::user()->userType == 3)
                        <li><a id="viewOrders" data-toggle="modal" data-target="#orders" class="btn"><span class="glyphicon glyphicon-align-center"> </span>Orders</a></li>
                    @endif
                <li ><a data-target="#" href="{{ route('ajaxusers.logout') }}" class="btn"><span class="glyphicon glyphicon-log-out"> </span> LOGOUT</a></li>
                @else
                <li ><a id="btn_createNewUser" data-toggle="modal" data-target="#signUpModal" class="btn"><span class="glyphicon glyphicon-user"> </span> SIGN UP</a></li>
                <li ><a id="btn_loginUser" data-toggle="modal" data-target="#loginModal" class="btn"><span class="glyphicon glyphicon-log-in"> </span> SIGN IN</a></li>
                @endif
                <li class="cart"><a class="btn cart-icon open-cart"><span class="glyphicon glyphicon-shopping-cart"><span class="badge badge2">0</span></span></a></li>
            </ul>
        </div>
    </nav>


    @include('layouts.navbar.login')
    @include('layouts.navbar.signup')
    @include('layouts.navbar.orders')

    <!--Cart--Modals-->
    <!--Cart--Modals Products-->
    @include('layouts.navbar.cart1')
    <!--Cart--Modals Personal Details-->
    @include('layouts.navbar.cart2')
    <!--Cart--Modals Review Order-->
    @include('layouts.navbar.cart3')
</header>
