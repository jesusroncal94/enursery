<div class="modal fade" id="cartModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Personal Details</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="has-feedback form-inline">
                        <div>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-edit"></span> </span>
                                <input type="text" id="cartModal2_name" class="form-control cart-Modal" placeholder="Enter Your Name Here*" value="{{ isset(Auth::user()->name) ? Auth::user()->name : '' }}"/>
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span> </span>
                                <input type="tel" id="cartModal2_tel" class="form-control cart-Modal" placeholder="Enter Your Mobile No. Here* " value="{{ isset(Auth::user()->contactNumber) ? Auth::user()->contactNumber : '' }}"/>
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-map-marker"></span> </span>
                                <input type="text" id="cartModal2_address" class="form-control cart-Modal" placeholder="Enter Your Address Here*" value="{{ isset(Auth::user()->address) ? Auth::user()->address : '' }}"/>
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span> </span>
                                <input type="email" id="cartModal2_email" class="form-control cart-Modal" placeholder="Enter Your Email Here*" value="{{ isset(Auth::user()->email) ? Auth::user()->email : '' }}"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a id="cartModal2_btnSaveAll" class="btn btn-next">SAVE & CONTINUE <span class="glyphicon glyphicon-arrow-right"></span></a>
                <button type="button" class="btn btn-default btn-prev">Prev</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
