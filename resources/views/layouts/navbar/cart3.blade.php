<div class="modal fade" id="cartModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Review Order</h4>
            </div>
            <div class="modal-body">
                <div>
                    <a class="btn btn-default btn-prev pull-right">Change</a>
                    <label>
                        <span id="nameModal3" class="glyphicon glyphicon-edit">

                        </span>
                    </label>
                    <label>
                        <span id="addressModal3" class="glyphicon glyphicon-map-marker">
                        </span>
                    </label>
                    <label>
                        <span id="telModal3" class="glyphicon glyphicon-phone">
                        </span>
                    </label>
                    <label>
                        <span id="emailModal3" class="glyphicon glyphicon-envelope">
                        </span>
                    </label>
                </div>
                <hr />
                <div>
                    <label>
                        <span class="glyphicon glyphicon-shopping-cart">
                            <b>Review Order Item(s)</b>
                        </span>
                    </label>
                    <a class="btn btn-default pull-right btn-change" id="cartModal3_btnChangeCart">Change</a>
                    <div class="table-responsive">
                        <table id="mycartPreview" class="table">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Name</th>
                                    <th>Qty</th>
                                    <th>Unit Price</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">
                                        <p>
                                            <span>
                                                <b>Item(s) Total</b><br />
                                                <small>Shipment Charges (Rs.25) </small>
                                            </span>
                                        </p>
                                    </td>
                                    <td colspan="1" align="left">
                                        <p>
                                            <b>$</b><b id="total" class="total">0</b><br />
                                            <small>$</small><small id="shipment" class="shipment">0</small>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="left"><p><b>Amount Payable </b></td>
                                    <td colspan="1" align="right"><p><b>$</b><b id="Payable" class="Payable">0</b></p></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a id="cartModal3_btnCreateOrder" class="btn btn-next">ORDER NOW <span class="glyphicon glyphicon-ok"></span></a>
                <button type="button" class="btn btn-default btn-prev">Prev</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
