<!--Login--Modal-->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="form_loginUser" class="modal-content animate">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title text-center" id="myModalLabel">Sign In</h4>
        </div>
        <div class="modal-body">
            <div class="logform">
                <div class="input-group">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-user"></span>
                    </span>
                    <input id="login_identifier" type="text" class="form-control" placeholder="Username or email*" name="identifier" required />
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-lock"></span>
                    </span>
                    <input id="login_password" type="password" class="form-control" placeholder="Password*" name="password" required />
                </div>
                <!-- <span class="pull-right" style="margin-right:10px;margin-top:5px;">
                    Forgot
                    <a href="#">password?</a>
                </span> -->
                <button id="login_btnSignIn" type="submit" class="submit btn  btn-success">Sign In</button>
                <label class="pull-right" style="margin-right:10px;margin-top:5px;">
                    <input type="checkbox" checked="checked" name="remember"> Remember me
                </label>
                <button id="login_btnSignUp" data-dismiss="modal" data-toggle="modal" data-target="#signUpModal" type="button" class="submit btn btn-default ">Sign Up</button>
            </div>
        </div>
    </form>
</div>
