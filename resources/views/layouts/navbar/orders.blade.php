///////////CSS///////////
<style>
    .font-Kalam {
        font-family: 'Kalam',cursive;
    }
    #orders tr {
        border-bottom:2px solid rgb(168, 101, 80) !important;
    }
    #orders h4, h5 {
        color: rgb(168, 101, 80) !important;
    }
    .modal-body {
        overflow-x: auto;
        overflow-y: auto;
    }
    .modal-dialog {
        max-width: 60% !important;
        width: 60% !important;
    }
</style>

<!-- Modal -->
<div id="orders" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center font-Kalam">{{ isset(Auth::user()->name) ? Auth::user()->name : '' }}</h4>
                <h5 class="modal-title text-center font-Kalam">Order History</h5>
            </div>
            <div class="modal-body">
                <table id="orders_table" class="table table-bordered">
                    <thead class="font-Kalam">
                        <tr style="background-color:rgb(168, 101, 80)">
                            <th class="text-center" scope="col">Order Id</th>
                            <th class="text-center" scope="col">Date</th>
                            <th class="text-center" scope="col">Product</th>
                            <th class="text-center" scope="col">Quantity</th>
                            <th class="text-center" scope="col">Amount(Rs.)</th>
                            <!-- <th class="text-center" scope="col">Partner</th> -->
                            <th class="text-center" scope="col">Status</th>
                            <!-- <th class="text-center" scope="col">Approved at</th>
                            <th class="text-center" scope="col">Processed at</th>
                            <th class="text-center" scope="col">Delivered at</th> -->
                        </tr>
                    </thead>
                    <tbody align="center">
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-center" colspan="3" scope="col">Total Page (Rs.)</th>
                            <th class="text-center" scope="col"></th>
                            <th class="text-center" scope="col"></th>
                            <!-- <th class="text-center" scope="col"></th> -->
                            <th class="text-center" scope="col"></th>
                            <!-- <th class="text-center" scope="col"></th>
                            <th class="text-center" scope="col"></th>
                            <th class="text-center" scope="col"></th> -->
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
