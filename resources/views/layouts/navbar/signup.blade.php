<!--SignUp--Modal-->
<div class="modal fade" id="signUpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-center" id="signUp_myModalLabel">Sign Up</h4>
            </div>
            <div class="modal-body">
                <form id="form_signUp" name="form_signUp" class="form-inline" >
                    <input type="hidden" name="user_id" id="signUp_user_id">
                    <input type="hidden" id="signUp_status_id" name="status_id" value="1"/>
                    <div class="signUpForm">
                        <div class="col-md-offset-0 col-md-6 col-xs-offset-2 col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </span>
                                <input id="signUp_name" type="text" class="form-control" placeholder="Name*" name="name" required />
                            </div>
                        </div>
                        <div class="col-md-offset-0 col-md-6 col-xs-offset-2 col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-phone"></span>
                                </span>
                                <input id="signUp_contactNumber" type="tel" class="form-control" placeholder="Contact No*" name="contactNumber" required />
                            </div>
                        </div>
                        <div class="col-md-offset-0 col-md-6 col-xs-offset-2 col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-envelope"></span>
                                </span>

                                <input id="signUp_email" type="email" class="form-control" placeholder="Email*" name="email" required />
                            </div>
                        </div>
                        <div class="col-md-offset-0 col-md-6 col-xs-offset-2 col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-map-marker"></span>
                                </span>
                                <input id="signUp_address" type="text" class="form-control" placeholder="Address*" name="address" required />
                            </div>
                        </div>
                        <div class="col-md-offset-0 col-md-6 col-xs-offset-2 col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-user"></span>
                                </span>
                                <input id="signUp_userName" type="text" class="form-control" placeholder="Username*" name="userName" required />
                            </div>
                        </div>
                        <div class="col-md-offset-0 col-md-6 col-xs-offset-2 col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-lock"></span>
                                </span>
                                <input id="signUp_password" type="password" class="form-control" placeholder="Password*" name="password" required />
                            </div>
                        </div>
                        <div class="col-md-offset-0 col-md-6 col-xs-offset-2 col-xs-8">
                            <!-- <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-lock"></span>
                                </span>
                                <input id="signUp_" type="password" class="form-control" placeholder="Confirm Password*" required />
                            </div> -->
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-list"></span> </span>
                                <select class="form-control" id="signUp_userType" name="userType">
                                    <option value='3'>Customer</option>
                                    <!-- <option value='2'>Partner</option>
                                    <option value='1'>Admin</option> -->
                                </select>
                            </div>
                        </div>
                        <button id="signUp_btnSignUp" type="submit" class="btn  btn-success ">Sign Up</button>
                        <button type="button" data-dismiss="modal" class="submit btn  btn-default ">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
