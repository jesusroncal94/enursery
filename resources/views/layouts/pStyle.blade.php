<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8" />
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href='https://fonts.googleapis.com/css?family=Kalam' rel='stylesheet'>
		<link href="assets/bstrapfiles/bootstrap-theme.css" rel="stylesheet" />
		<link href="assets/bstrapfiles/bootstrap.css" rel="stylesheet" />
		<link href="assets/bstrapfiles/main.css" rel="stylesheet" />
		<link href="assets/appfiles/pNursery.css" rel="stylesheet" />
		<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

		<style>
		.dataTables_paginate a {
				padding: 6px 9px !important;
				background: #123132 !important;
				border-color: #ddd !important;
		}

		table,
		thead,
		th {
		  	color: black !important;
		}
		</style>

		<title>{{ $title }}</title>
</head>
<body>
		<div>
				<div id="header">
						<div class="header-part1 col-xs-5 col-sm-4 col-md-3">
						</div>
						@yield('header')
				</div>

				@yield('sidebar')

				<div class="tab-content ">
						@yield('content')
				</div>

        <footer>
						@yield('footer')
				</footer>
		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="assets/appfiles/pNursery.js"></script>
		<script src="assets/jsfiles/bootstrap.js"></script>
		<!-- <script src="assets/jsfiles/jquery-1.9.1.js"></script> -->
		<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
		<script src="//cdn.rawgit.com/ashl1/datatables-rowsgroup/v1.0.0/dataTables.rowsGroup.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.15.1/dist/sweetalert2.all.min.js"></script>

		<script>
		//Show a sweetalert2 in success case
		function showSuccess(data)
		{
				console.log('Success:', data);
				var message = '';
				$.each(data.data, function(index, value) {
						message += index + ': ' + value + ', ';
				});
				Swal.fire({
						position: 'center',
						icon: 'success',
						title: data.message,
						text: message.substring(0, message.length - 2),
						showConfirmButton: true,
				});
		}


		//Show a sweetalert2 in error case
		function showError(data)
		{
				console.log('Error:', data);
				var message = '';
				$.each(data.responseJSON.data, function(index, value) {
						message += index + ': ' + value + ', ';
				});
				Swal.fire({
						position: 'center',
						icon: 'error',
						title: data.responseJSON.message,
						text: message.substring(0, message.length - 2),
						showConfirmButton: true,
				});
		}
		</script>

		@yield('scripts')
</body>
</html>
