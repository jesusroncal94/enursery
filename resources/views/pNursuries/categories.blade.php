<div class="tab-pane fade" id="addCat">
    <div class="col-xs-offset-1 col-xs-10">
        <div class="row">
            <div>
                <div class="registration-info">
                    <h1>
                        Categories
                    </h1>
                    <p>
                        Categories on {{ date("F j, Y, g:i a") }}
                    </p>
                </div>
                <div class="table-background">
                    <table id="categories" class="table text-center data-table" width="100%">
                        <thead>
                            <tr>
                                <th colspan="3" style="cursor:pointer;">
                                    <a id="btn_createNewCategory" style="width:100%;">
                                        <span style="margin-left:25px; color:rgb(18, 49, 50)" class="pull-left">Add New</span>
                                        <i style="margin-right:25px; color:rgb(18, 49, 50)" class="fa fa-plus pull-right"></i>
                                    </a>
                                </th>
                            </tr>
                            <tr>
                                <th class="text-center">Image</th>
                                <th class="text-center">Category</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
