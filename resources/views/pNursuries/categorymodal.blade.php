<div class="modal fade" id="addnewCat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="text-center order-details">Add New Category</h4>
            </div>
            <form id="categoryForm" name="categoryForm" class="form-horizontal" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="addnewCat">
                    <input type="hidden" name="category_id" id="category_id">
                    <div class="has-feedback form-inline">
                        <div>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-edit"></span> </span>
                                <input type="text" id="catName" name="catName" class="form-control" placeholder="Type Category Name Here*" />
                            </div>
                            <div id="div_current_image" class="input-group">
                            </div>
                            <div class="input-group">
                                <input type="file" id="image" name="image" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-sm update-profile" data-dismiss="modal"> Cancel</a>
                <button type="submit" id="btn_saveCategory" class="btn btn-sm update-profile">Save Category</button>
            </div>
            </form>
        </div>
    </div>
</div>
