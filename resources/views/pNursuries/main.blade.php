@extends('layouts.pStyle', [
    'title' => 'Partner Panel - Enursery'
])


@section('header')
    @include('pNursuries.productmodal')
    @include('pNursuries.categorymodal')
    @include('pNursuries.ordermodal')
@stop


@section('sidebar')
    @include('pNursuries.sidebar')
@stop


@section('content')
    @include('pNursuries.dashboard')
    @include('pNursuries.profile')
    @include('pNursuries.products')
    @include('pNursuries.categories')
    @include('pNursuries.orders')
    @include('pNursuries.neworders')
@stop


@section('footer')
    @include('pNursuries.footer')
@stop


@section('scripts')
<script>
$(document).ready(function () {
    //CSRF TOKEN
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    // Categories (start) ******************************
    // Categories DataTable
    var categories_table = $('#categories').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('ajaxcategories.index') }}',
        columns: [
            {data: 'image_upload', name: 'image_upload'},
            {data: 'catName', name: 'catName'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    // Create New Category (references to categorymodal)
    $('#btn_createNewCategory').click(function () {
        $('#category_id').val('');
        $('#categoryForm').trigger("reset");
        $('#div_current_image').hide();
        $('#div_current_image').html('');
        $('#addnewCat').modal('show');
    });

    // Edit category (references to categorymodal)
    $('body').on('click', '.editCategory', function () {
        var category_id = $(this).data('id');
        $('#div_current_image').html('');
        $.get("{{ route('ajaxcategories.index') }}" +'/' + category_id +'/edit', function (data) {
            $('#addnewCat').modal('show');
            $('#category_id').val(data.id);
            $('#catName').val(data.catName);
            $('#div_current_image').show();
            var url = '{{ URL::asset("images/categories") }}';
            var url_image = url.concat('/', data.image);
            $('#div_current_image').html('<span class="input-group-addon"><span class="fa fa-file-image-o"></span> </span> <img id="current_image" src=\"'+url_image+'\" width="200px" height="140px">');
        })
    });

    // Save category (references to categorymodal)
    $("#categoryForm").on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            url: "{{ route('ajaxcategories.store') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('categoryForm').trigger("reset");
                $('#addnewCat').modal('hide');
                categories_table.draw();
                data.success == true ? showSuccess(data) : showError(data);
            },
            error: function (data) {
                showError(data);
            }
        });
    });
    // Categories (end) ******************************


    // Products (start) ******************************
    // Products DataTable
    var products_table = $('#products').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('ajaxproducts.index') }}",
            type: 'GET',
            data: {productUser_id: '{{ Auth::user()->id }}', status_id: 1},
        },
        columns: [
            {data: 'productImage_upload', name: 'productImage_upload'},
            {data: 'productName', name: 'productName'},
            {data: 'productDescription', name: 'productDescription'},
            {data: 'stock[0].pivot.productQuantity', name: 'stock[0].pivot.productQuantity', "render": function (data, type, row) {
                    if(row.stock == "") {
                        return "0";
                    } else {
                        return row.stock[0].pivot.productQuantity;
                    }
                },
            },
            {data: 'productPrice', name: 'productPrice'},
            {data: 'updated_at', name: 'updated_at'},
            {data: 'category.catName', name: 'category.catName'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    // Create New product (references to productmodal)
    $('#btn_createNewProduct').click(function () {
        $('#title_modalProduct').text('New Product');
        $('#product_id').val('');
        $('#productForm').trigger("reset");
        $('#productImage').val('');
        loadCategories();
        $('#div_current_productImage').hide();
        $('#div_current_productImage').html('');
        $('#addNewProduct').modal('show');
    });

    // Edit product (references to productmodal)
    $('body').on('click', '.editProduct', function () {
        $('#title_modalProduct').text('Edit Product');
        var product_id = $(this).data('id');
        $('#div_current_productImage').html('');
        $.ajax({
            data: {user_id: '{{ isset(Auth::user()->id) ? Auth::user()->id : "" }}'},
            url: "{{ route('ajaxproducts.index') }}" +'/' + product_id +'/edit',
            type: "GET",
            dataType: 'json',
            success: function (data) {
                $('#addNewProduct').modal('show');
                $('#product_id').val(data.id);
                $('#productName').val(data.productName);
                $('#productDescription').val(data.productDescription);
                if(data.stock.length != 0)
                {
                    $('#productQuantity').val(data.stock[0].pivot.productQuantity);
                } else {
                    $('#productQuantity').val(0);
                }
                $('#productPrice').val(data.productPrice);
                loadCategories(data.productCategory);
                $('#div_current_productImage').show();
                var url = '{{ URL::asset("images/products") }}';
                var url_image = url.concat('/', data.productImage);
                $('#div_current_productImage').html('<span class="input-group-addon"><span class="fa fa-file-image-o"></span> </span> <img id="current_productImage" src=\"'+url_image+'\" width="200px" height="140px">');
            },
            error: function (data) {
                showError(data);
            }
        });
        // $.get("{{ route('ajaxproducts.index') }}" +'/' + product_id +'/edit', function (data) {
        //     $('#addNewProduct').modal('show');
        //     $('#product_id').val(data.id);
        //     $('#productName').val(data.productName);
        //     $('#productDescription').val(data.productDescription);
        //     $('#productQuantity').val(data.stock[0].pivot.productQuantity);
        //     $('#productPrice').val(data.productPrice);
        //     loadCategories(data.productCategory);
        //     $('#div_current_productImage').show();
        //     var url = '{{ URL::asset("images/products") }}';
        //     var url_image = url.concat('/', data.productImage);
        //     $('#div_current_productImage').html('<span class="input-group-addon"><span class="fa fa-file-image-o"></span> </span> <img id="current_productImage" src=\"'+url_image+'\" width="200px" height="140px">');
        // });
    });

    // Save product (references to productmodal)
    $("#productForm").on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            url: "{{ route('ajaxproducts.store') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('productForm').trigger("reset");
                $('#productImage').val('');
                $('#addNewProduct').modal('hide');
                products_table.draw();
                data.success == true ? showSuccess(data) : showError(data);
            },
            error: function (data) {
                showError(data);
            }
        });
    });
    // Products (end) ******************************


    // Profile (start) ******************************
    // Save/Update User (admin)
    $("#form_profile").on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            url: "{{ route('ajaxusers.store') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                data.success == true ? showSuccess(data) : showError(data);
            },
            error: function (data) {
                showError(data);
            }
        });
    });
    // Profile (end) ******************************


    // New Order (start) ******************************
    // New Orders DataTable
    var new_orders_table = $('#newOrder_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('ajaxorders.index') }}",
            type: 'GET',
            data: {
                userType: '{{ isset(Auth::user()->userType) ? Auth::user()->userType : "" }}',
                partner_id: '{{ isset(Auth::user()->id) ? Auth::user()->id : "" }}',
                view: 'neworders',
            },
        },
        columns: [
            // {data: 'id', name: 'id', "render": function (data, type, row) {
            //         return 'ord000'+row.id;
            //     },
            // },
            {data: 'id', name: 'id'},
            {data: 'idFormat', name: 'idFormat'},
            {data: 'created_at', name: 'created_at'},
            {data: 'order_details', name: 'order_details', "render": function (data, type, row) {
                    return row.order_details.length;
                },
            },
            //{data: 'total', name: 'total'},
            {data: 'total', name: 'total'},
            {data: 'partner.name', name: 'partner.name', defaultContent: '-'},
            {data: 'status.display_name', name: 'status.display_name'},
            {data: 'approved_at', name: 'approved_at', defaultContent: '-'},
            {data: 'processed_at', name: 'processed_at', defaultContent: '-'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        order: [[ 2, "desc" ]]
    });

    // View Order Details (references to ordermodal)
    $('body').on('click', '.viewOrder', function () {
        $('#orderDetail_title').text('Order Detail');
        var order_id = $(this).data('id');

        $.get("{{ route('ajaxorders.index') }}" +'/' + order_id +'/edit', function (data) {
            console.log(data);
            var personalDetails = JSON.parse(data.personalDetails);
            var orderDetails = data.order_details;
            var numItems = orderDetails.length;
            console.log(orderDetails);
            $('#orderDetail').modal('show');
            $('#orderDetail_order_id').text('Ord-'+String(data.id).padStart(6, '0'));
            $('#orderDetail_created_at').text(data.created_at);
            $('#orderDetail_customer').text(personalDetails.name);
            $('#orderDetail_email').text(personalDetails.email);
            $('#orderDetail_contactNumber').text(personalDetails.contactNumber);
            $('#orderDetail_address').text(personalDetails.address);

            $("#orderDetail tbody").html('');
            for (var i = 0; i < numItems; i++) {
                var url = '{{ URL::asset("images/products") }}';
                var url_image = url.concat('/', orderDetails[i].productImage);
                var productImage = url_image;
                $("#orderDetail tbody").append('<tr>'+
                '<td><div class="product-img"><div class="img-prdct"><img src=\"'+productImage+'\"></div></div></td>'+
                '<td>'+orderDetails[i].productName+'</td>'+
                '<td>'+orderDetails[i].pivot.productQuantity+'</td>'+
                '<td>'+orderDetails[i].pivot.productPrice+'</td>'+
                '<td align="center">'+orderDetails[i].pivot.productTotalPrice+'</td>'+
                '</tr>');
            }

            $('#orderDetail_subtotal').text(data.subtotal);
            $('#orderDetail_shipment').text(data.shipment);
            $('#orderDetail_total').text(data.total);
        });
    });

    // Process new Order
    $('body').on('click', '.processOrder', function () {
        var id = $(this).data('id');
        var order_id = 'Ord-'+String($(this).data('id')).padStart(6, '0');
        Swal.fire({
            title: 'Are you sure you process the order?',
            text: "Order: "+order_id,
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, process it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    data: {order_id: id, user_id: '{{ isset(Auth::user()->id) ? Auth::user()->id : "" }}'},
                    url: "{{ route('ajaxorders.processOrder') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        data.success == true ? showSuccess(data) : showError(data);
                        new_orders_table.draw();
                    },
                    error: function (data) {
                        showError(data);
                    }
                });
            }
        });
    });

    // Delivere new Order
    $('body').on('click', '.delivereOrder', function () {
        var id = $(this).data('id');
        var order_id = 'Ord-'+String($(this).data('id')).padStart(6, '0');
        Swal.fire({
            title: 'Are you sure you delivere the order?',
            text: "Order: "+order_id,
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delivere it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    data: {order_id: id, user_id: '{{ isset(Auth::user()->id) ? Auth::user()->id : "" }}'},
                    url: "{{ route('ajaxorders.delivereOrder') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        data.success == true ? showSuccess(data) : showError(data);
                        new_orders_table.draw();
                    },
                    error: function (data) {
                        showError(data);
                    }
                });
            }
        });
    });
    // New Order (end) ******************************


    // Order (start) ******************************
    // Orders DataTable
    var orders_table = $('#totalOrders_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('ajaxorders.index') }}",
            type: 'GET',
            data: {
                userType: '{{ isset(Auth::user()->userType) ? Auth::user()->userType : "" }}',
                partner_id: '{{ isset(Auth::user()->id) ? Auth::user()->id : "" }}',
                view: 'orders',
            },
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'idFormat', name: 'idFormat'},
            {data: 'created_at', name: 'created_at'},
            {data: 'order_details', name: 'order_details', "render": function (data, type, row) {
                    var html = '';
                    for (var i = 0; i < row.order_details.length; i++) {
                        html += '<p>* '+row.order_details[i].productName+'</p>';
                    }
                    return html;
                },
            },
            {data: 'order_details', name: 'order_details', "render": function (data, type, row) {
                    var html = '';
                    for (var i = 0; i < row.order_details.length; i++) {
                        html += '<p>* '+row.order_details[i].pivot.productQuantity+'</p>';
                    }
                    return html;
                },
            },
            {data: 'total', name: 'total'},
            {data: 'partner.name', name: 'partner.name', defaultContent: '-'},
            {data: 'status.display_name', name: 'status.display_name'},
            {data: 'approved_at', name: 'approved_at', defaultContent: '-'},
            {data: 'processed_at', name: 'processed_at', defaultContent: '-'},
            {data: 'delivered_at', name: 'delivered_at', defaultContent: '-'},
        ],
        order: [[ 10, "desc" ]],
        footerCallback: function(row, data, start, end, display) {
            var api = this.api(), data;
            // Remove the formatting to get integer data for summation
            var intVal = function(i) {
                return typeof i === 'string'
                    ? Number(i)
                    : typeof i === 'number'
                        ? i
                        : 0;
            };
            // Total over this page
            pageTotal = api
                .column(5, {page: 'current'})
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            // Update footer
            $( api.column(5).footer() ).html(
                pageTotal
            );
        },
    });
    // Order (end) ******************************


    // Dashboard (start) ******************************
    $("#form_dashboard").on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            data: $(this).serialize(),
            url: "{{ route('ajaxdashboards.index') }}",
            type: "GET",
            dataType: 'json',
            success: function (data) {
                $('#subservs').hide(100);

                $orders_total = data.data.orders[0].total_orders;
                $orders_cant_month = data.data.orders[0].month;
                $orders_percentage_change = ($orders_total - $orders_cant_month != 0) ? (($orders_total / ($orders_total - $orders_cant_month)) * 100) - 100 : 100;
                if($orders_percentage_change > 0)
                {
                    $('#dashboard_orders').html(
                        '('+$orders_cant_month+') <br>'+
                        '<span style="font-size:15px; color:green" class="fa fa-arrow-up" id="dashboard_orders_percentage_change">'+$orders_percentage_change+'%</span>'
                    );
                } else if ($orders_percentage_change == 0)
                {
                    $('#dashboard_orders').html(
                        '('+$orders_cant_month+') <br>'+
                        '<span style="font-size:15px; color:yellow" class="fa fa-arrow-right" id="dashboard_orders_percentage_change">'+$orders_percentage_change+'%</span>'
                    );
                } else
                {
                    $('#dashboard_orders').html(
                        '('+$orders_cant_month+') <br>'+
                        '<span style="font-size:15px; color:red" class="fa fa-arrow-down" id="dashboard_orders_percentage_change">'+$orders_percentage_change+'%</span>'
                    );
                }
                $('#dashboard_orders_total').text($orders_total);

                $sale_total = data.data.orders[0].total_cant_products;
                $sale_cant_month = data.data.orders[0].month_cant_products;
                $sale_percentage_change = ($sale_total - $sale_cant_month != 0) ? (($sale_total / ($sale_total - $sale_cant_month)) * 100) - 100 : 100;
                if($sale_percentage_change > 0)
                {
                    $('#dashboard_sale').html(
                        '('+$sale_cant_month+') <br>'+
                        '<span style="font-size:15px; color:green" class="fa fa-arrow-up" id="dashboard_sale_percentage_change">'+$sale_percentage_change+'%</span>'
                    );
                } else if ($sale_percentage_change == 0)
                {
                    $('#dashboard_sale').html(
                        '('+$sale_cant_month+') <br>'+
                        '<span style="font-size:15px; color:yellow" class="fa fa-arrow-right" id="dashboard_sale_percentage_change">'+$sale_percentage_change+'%</span>'
                    );
                } else
                {
                    $('#dashboard_sale').html(
                        '('+$sale_cant_month+') <br>'+
                        '<span style="font-size:15px; color:red" class="fa fa-arrow-down" id="dashboard_sale_percentage_change">'+$sale_percentage_change+'%</span>'
                    );
                }
                $('#dashboard_sale_total').text($sale_total);

                $('#subservs').show(1000);
            },
            error: function (data) {
                showError(data);
            }
        });
    });

    var year = '{{ date("Y") }}';
    var month = '{{ date("m") }}'.replace(/^0+/, '');
    $("#dashboard_year option[value="+year+"]").attr('selected', 'selected');
    $("#dashboard_month option[value="+month+"]").attr('selected', 'selected');
    $("#form_dashboard").trigger('submit');
    // Dashboard (end) ******************************
});

function loadCategories(category_id = 0)
{
    var select = $('#productCategory');
    select.empty().append("<option value=0>.:Select Category</option>");

    $.ajax({
        data: {},
        url: "{{ route('ajaxcategories.get') }}",
        type: "GET",
        dataType: 'json',
        success: function (data) {
          $.each(data, function(index, value) {
              if (value.id == category_id)
              {
                  select.append('<option selected value=' + value.id + '>' + value.catName + '</option>');
              } else
              {
                  select.append('<option value=' + value.id + '>' + value.catName + '</option>');
              }
          });
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}

</script>
@stop
