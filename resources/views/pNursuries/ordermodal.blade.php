<div class="modal fade" id="orderDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="text-center order-details" id="orderDetail_title"></h4>
            </div>
            <div class="modal-body">

                <div style="margin-left:10px">
                    <p><b class="order-details">Order Id: </b><span id="orderDetail_order_id"></span></p>
                    <p><b class="order-details">Date-time: </b><span id="orderDetail_created_at"></span></p>
                    <p><b class="order-details">Customer: </b><span id="orderDetail_customer"></span></p>
                    <p><b class="order-details">Email: </b><span id="orderDetail_email"></span></p>
                    <p><b class="order-details">Contact: </b><span id="orderDetail_contactNumber"></span></p>
                    <p><b class="order-details">Shipment Address: </b><span id="orderDetail_address"></span></p>
                </div>
                <div>
                    <div class="table-responsive" style="border:1px solid black">
                        <table id="mycart1" class="table" style="border:none">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Name</th>
                                    <th>Qty</th>
                                    <th>Unit Price (Rs.)</th>
                                    <th>Total Price (Rs.)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- <tr>
                                    <td>
                                        <div class="product-img">
                                            <div class="img-prdct"><img src="{{ URL::asset('images') }}/Capture4.PNG"></div>
                                        </div>
                                    </td>
                                    <td>
                                        <p>Product One</p>
                                    </td>
                                    <td>
                                        <label>5</label>
                                    </td>
                                </tr> -->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">
                                        <p>
                                            <span>
                                                <b>Item(s) Total</b><br />
                                                <small>Shipment Charges </small>
                                            </span>
                                        </p>
                                    </td>
                                    <td colspan="1" align="center">
                                        <p>
                                            <b>Rs. </b><b id="orderDetail_subtotal" class="total"></b><br />
                                            <small>Rs. </small><small id="orderDetail_shipment" class="shipment"></small>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="left"><p><b>Amount Payable </b></td>
                                    <td colspan="1" align="center"><p><b>Rs. </b><b id="orderDetail_total" class="Payable"></b></p></td>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="pull-right view-Order-a">
                            <a class="btn btn-sm"><i class="fa fa-check"></i></a>
                            <a class="btn btn-sm"><i class="fa fa-close"></i></a>
                            <a class="btn btn-sm"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
