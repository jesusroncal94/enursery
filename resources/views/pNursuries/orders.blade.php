<div class="tab-pane fade" id="totalOrders">
    <div class="col-xs-offset-1 col-xs-10">
        <div class="row">
            <div>
                <div class="registration-info">
                    <h1>
                        Total Orders
                    </h1>
                    <p>
                        Total Orders {{ date("F j, Y, g:i a") }}
                    </p>
                </div>
                <div class="table-background">
                    <table id="totalOrders_table" class="table text-center">
                        <thead>
                            <tr>
                                <th class="text-center" scope="col">Id</th>
                                <th class="text-center" scope="col">Order Id</th>
                                <th class="text-center" scope="col">Date</th>
                                <th class="text-center" scope="col">Product</th>
                                <th class="text-center" scope="col">Quantity</th>
                                <th class="text-center" scope="col">Amount(Rs.)</th>
                                <th class="text-center" scope="col">Partner</th>
                                <th class="text-center" scope="col">Status</th>
                                <th class="text-center" scope="col">Approved at</th>
                                <th class="text-center" scope="col">Processed at</th>
                                <th class="text-center" scope="col">Delivered at</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- <tr>
                                <td scope="row">ord102</td>
                                <td>10 june 2020 </td>
                                <td>
                                    Sunflower<br />
                                    Mango<br />
                                    Rose<br />
                                    Carry<br />

                                </td>
                                <td>
                                    20<br />
                                    5<br />
                                    4<br />
                                    1<br />
                                </td>
                                <td>25485</td>
                                <td>Delivered</td>
                            </tr> -->
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-center" colspan="4" scope="col">Total Page (Rs.)</th>
                                <th class="text-center" scope="col"></th>
                                <th class="text-center" scope="col"></th>
                                <th class="text-center" scope="col"></th>
                                <th class="text-center" scope="col"></th>
                                <th class="text-center" scope="col"></th>
                                <th class="text-center" scope="col"></th>
                                <th class="text-center" scope="col"></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
