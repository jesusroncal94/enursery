<div class="modal fade" id="addNewProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="text-center order-details" id="title_modalProduct"></h4>
            </div>
            <form id="productForm" name="productForm" class="form-horizontal" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="addnewProduct">
                    <input type="hidden" name="product_id" id="product_id">
                    <input type="hidden" name="productUser_id" id="productUser_id" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="productStatus_id" id="productStatus_id" value="1">
                    <div class="has-feedback form-inline">
                        <div>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-edit"></span> </span>
                                <input type="text" id="productName" name="productName" class="form-control" placeholder="Type Product Name Here*" />
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-info-circle"></span> </span>
                                <input type="text" id="productDescription" name="productDescription" class="form-control" placeholder="Type Product Description Here*" />
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-sort-amount-desc"></span> </span>
                                <input type="number" id="productQuantity" name="productQuantity" min="0" class="form-control" placeholder="Enter Product Quantity Here*" />
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-bitcoin"></span> </span>
                                <input type="number" id="productPrice" name="productPrice" min="0" class="form-control" placeholder="Enter Product Price Here*" />
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-list"></span> </span>
                                <select class="form-control" id="productCategory" name="productCategory">
                                </select>
                            </div>
                            <div id="div_current_productImage" class="input-group">
                            </div>
                            <div class="input-group">
                                <input type="file" id="productImage" name="productImage"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-sm update-profile" data-dismiss="modal"> Cancel</a>
                <button type="submit" id="btn_saveProduct" class="btn btn-sm update-profile">Save Product</button>
            </div>
            </form>
        </div>
    </div>
</div>
