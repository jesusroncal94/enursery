<div class="tab-pane fade" id="totalPlants">
    <div class="col-xs-offset-1 col-xs-10">
        <div class="row">
            <div>
                <div class="registration-info">
                    <h1>
                        Plants
                    </h1>
                    <p>
                        Total Plants on {{ date("F j, Y, g:i a") }}
                    </p>
                </div>
                <div class="table-background">
                    <table id="products" class="table text-center data-table" width="100%">
                        <thead>
                            <tr>
                                <th colspan="8" style="cursor:pointer;">
                                    <a id="btn_createNewProduct" style="width:100%;">
                                        <span style="margin-left:25px; color:rgb(18, 49, 50)" class="pull-left">Add New</span>
                                        <i style="margin-right:25px; color:rgb(18, 49, 50)" class="fa fa-plus pull-right"></i>
                                    </a>
                                </th>
                            </tr>
                            <tr>
                                <th class="text-center" scope="col">Image</th>
                                <th class="text-center" scope="col">Name</th>
                                <th class="text-center" scope="col">Description</th>
                                <th class="text-center" scope="col">Quantity</th>
                                <th class="text-center" scope="col">Price</th>
                                <th class="text-center" scope="col">Last Update</th>
                                <th class="text-center" scope="col">Category</th>
                                <th class="text-center" scope="col">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
