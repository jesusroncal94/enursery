<div class="tab-pane fade" id="profile">
    <form id="form_profile" class="form-inline form-group" enctype="multipart/form-data">
        <div class="col-xs-12 col-md-offset-1 col-md-9 col-sm-9">
            <div class="profile">
                <div class="profile-info">
                    <h1>
                        Edit Profile
                    </h1>
                    <p>
                        Complete your profile
                    </p>
                </div>
                <input type="hidden" id="profile_user_id" name="user_id" value="{{ Auth::user()->id }}">
                <input type="hidden" id="profile_userType" name="userType" value="{{ Auth::user()->userType }}">
                <input type="hidden" id="profile_status_id" name="status_id" value="{{ Auth::user()->status_id }}">
                <div class="input-group">
                    <input id="profile_name" name="name" type="text" class="form-control" required value="{{ Auth::user()->name }}" />
                    <span class="floating-label">First Name*</span>
                </div>
                <div class="input-group">
                    <input id="profile_contactNumber" name="contactNumber" type="text" class="form-control" placeholder="" required value="{{ Auth::user()->contactNumber }}"/>
                    <span class="floating-label">Contct No*</span>
                </div>
                <div class="input-group">
                    <input id="profile_email" name="email" type="text" class="form-control" placeholder="" required value="{{ Auth::user()->email }}"/>
                    <span class="floating-label">Email*</span>
                </div>
                <div class="input-group">
                    <input id="profile_userName" name="userName" type="text" class="form-control" placeholder="" required value="{{ Auth::user()->userName }}"/>
                    <span class="floating-label">Username*</span>
                </div>
                <div class="input-group">
                    <input id="profile_password" name="password" type="text" class="form-control" placeholder="" />
                    <span class="floating-label">Password*</span>
                </div>
                <div class="input-group address">
                    <input id="profile_address" name="address" type="text" class="form-control" placeholder="" required value="{{ Auth::user()->address }}"/>
                    <span class="floating-label">Address*</span>
                </div>
                <div class="modal-footer" style="border:none">
                    <button id="profile_btnSaveUser" class="btn btn-sm update-profile"> Update Profile</button>
                </div>
            </div>
        </div>
        <div class="hidden-xs col-md-2 col-sm-3 profile-part2">
            <div>
                <div class="avatar-upload">
                    <div class="avatar-edit">
                        <input type='file' id="imageUpload" name="image" accept=".png, .jpg, .jpeg" />
                        <label for="imageUpload"></label>
                    </div>
                    <div class="avatar-preview">
                        @if(Auth::user()->image)
                            <div id="imagePreview" style="background-image: url('/images/users/{{ Auth::user()->image }}');">
                            </div>
                        @else
                            <div id="imagePreview">
                            </div>
                        @endif
                    </div>
                </div>
                <div class="manager-details">
                    <h3>{{ Auth::user()->name }} <small>(Partner)</small></h3>
                    <h5>{{ Auth::user()->contactNumber }}</h5>
                    <h5>{{ Auth::user()->email }}</h5>
                </div>
            </div>
        </div>
    </form>
</div>
