@extends('layouts.app', [
		'title' => 'Enursery: Products'
])


@section('header')
		@include('product.header')
@stop


@section('content')
		@include('product.products')
@stop


@section('scripts')
@stop
