<section id="viewAllPrduct">
    <section>
        @forelse ($products as $product)
        <section class="col-lg-4 col-md-4 col-sm-offset-0 col-sm-6 col-xs-offset-2 col-xs-8">
            <div class="psum-item">
                <div class="image-container">
                    <a onclick="showProductDetails({{ $product->id }});">
                        <img class="psum-image img-responsive" src="{{ URL::asset('images/products') }}/{{ $product->productImage }}" alt="product" />
                    </a>
                    <div class="overlay">
                        <a onclick="showProductDetails({{ $product->id }});" class="btn quickview-btn">QUICK VIEW</a>
                        <button onclick="addToCart({{ $product->id }});" href="" class="btn overlay-btn cart-btn">ADD TO CART</button>
                    </div>
                </div>
                <div class="psum-info">
                    <h2>{{ $product->productName }}</h2>
                    <p>
                        <span class=""> Rs.{{ $product->productPrice }}/-</span>
                    </p>
                </div>
            </div>
        </section>
        @empty
            <p>No products</p>
        @endforelse

      	<div class="col-xs-12">
      		  <a id="load" class="btn loadMore-btn">LOAD MORE</a>
      	</div>
    </section>


    <div id="details" class="modal text-center">
      	<div class="modal-dialog modal-lg">
        		<div class="modal-content modal-lg animate" method="post">
          			<div class="panel-body">
            				<span style="margin-right:0px; margin-top:-8px; " onclick="document.getElementById('details').style.display='none'" class="close" title="Close Modal">&times;</span>
            				<div class="col-xs-12">
              					<div class="col-xs-5">
                						<img class="img-responsive" src="" id="productImage" />
                						<span>Total: Rs.<span class="t-bill" id="productTotalPrice"></span> /-</span>
              					</div>
              					<div class="col-xs-7" style="margin-top:-10px;">
                						<h3 id="productName"></h3>
                						<h4 class="psum-name-details">Rs.<span class="bill" id="productPrice"></span> /-</h4>
                						<p id="productDescription"></p>
                						<div class="has-feedback form-inline">
                  							<div class="input-group">
                      						  <input id="productQuantity" type="number" min="0" class="form-control cart-quant" value="0" />
                  							</div>
                						</div>
                						<button onclick="addToCart({{ $product->id }});" href="" class="btn overlay-btn cart-btn">ADD TO CART</button>
              					</div>
            				</div>
          			</div>
        		</div>
      	</div>
    </div>
</section>
