<!--About--Section-->
<section id="about" class="container">
    <div class="col-lg-12">
        <div class="col-lg-6">
            <h2 class="mainheading">
                about us
            </h2>
            <div class="col-lg-8 col-lg-offset-2">
                <p>
                    <b>enursery</b>  landscape design and maintenance firm offering our clients the benefit and convenience of working with a single contractor for all their landscape desires. we take great pride in tailoring consistent, high quality services and products to our clientele.
                </p>
            </div>
        </div>
        <div class="col-lg-6">
            <img class="about-image" src="assets/banners/garden-5061193_1920.jpg" />
        </div>
        <div class="col-lg-6">
            <img class="about-image" src="images/Capture2.PNG" />
        </div>
        <div class="col-lg-6 about">
            <h2 class="about-content">
                BUY ONLINE NOW & GET 10% OFF !
            </h2>
            <a href="viewAllProducts.html" class="btn viewAll">SHOP NOW</a>
        </div>
    </div>
</section>
