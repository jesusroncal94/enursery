<!--Contact--Section-->
<section id="contact">
    <div class="container">
        <div class="col-xs-12">
            <h1 class="mainheading">
                You're Welcome to Visit
            </h1>
            <h4>Have a Question? We're Here to Help </h4>
            <div class="">
                <p>
                    Email us at
                    <a href="eNursery@gmail.com">eNursery@gmail.com</a>
                    or send us a message via the contact form below and we'll get back to you
                </p>
            </div>
            <div class="form-group">
                <div class="has-feedback form-inline">
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span> </span>
                        <input id="contact_name" type="text" class="form-control" placeholder="Enter Your Name Here*" />
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span> </span>
                        <input id="contact_email" type="text" class="form-control" placeholder="Enter Your Email Here*" />
                    </div>
                </div>
                <textarea rows="10" style="margin-top:10px;width:70%; min-width:400px" placeholder="Type your message here*" ;></textarea>
                <a class="btn btn-send" style="">SEND</a>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h3>OUR STORES</h3>
                    <hr style="left:0px; margin:auto; width:100px;border-top:solid 2px" />
                    <div class="text-center">
                        <p>
                            500 Terry Francois Street San
                            Francisco, CA 94158<br />
                            Tel: +92-000-0000000
                        </p>
                        <p>
                            500 Terry Francois Street San
                            Francisco, CA 94158<br />
                            Tel: +92-000-0000000
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <h3>OPENING HOURS</h3>
                    <hr style="left:0px; margin:auto; width:100px;border-top:solid 2px" />
                    <div class="text-center">
                        <p>
                            Mon - Fri: 8am - 8pm<br />
                            Saturday: 9am - 9pm<br />
                            Sunday: 9am - 10pm<br />
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <h3>HELP</h3>
                    <hr style="left:0px; margin:auto; width:100px;border-top:solid 2px" />
                    <div class="text-center">
                        <a href="#">Shipping & Return</a><br />
                        <a href="#">Privacy Policy</a><br />
                        <a href="#">FAQ</a><br />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <img class="img-responsive" src="assets/banners/seedlings-5061179_1920.jpg" />
    </div>
</section>
