<!--FAQ--Section-->
<section id="faq" class="container">
    <div class="col-md-6 col-md-offset-3">
        <div style="margin-top:0px;">
            <h2 class="mainheading">
                FAQ
            </h2>
            <div class="col-lg-8 col-lg-offset-2">
                <h4 style="text-align:center">
                    Frequently Asked Questions...
                </h4>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="faq-Section">
            <a id="faqQ1">
                <h5 class=" panel-title">
                    <span>How do I add a new question?</span>
                    <span class="glyphicon glyphicon-chevron-down pull-right"></span>
                </h5>
            </a>
            <div class="panel-body">
                <div id="faqA1" class="faq-ans">
                    <p>To add a new question go to app settings and press "Manage Questions" button.</p>
                    <a href="#" class="fa fa-facebook"></a>
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-google"></a>
                    <a href="#" class="fa fa-youtube"></a>
                </div>
            </div>
        </div>
        <div class="faq-Section">
            <a id="faqQ2">
                <h5 class="panel-title">
                    <span>Can I insert pictures in my FAQ?</span>
                    <span class="glyphicon glyphicon-chevron-down pull-right"></span>
                </h5>
            </a>
            <div class="panel-body">
                <div id="faqA2" class="faq-ans">
                    <p>
                        Yes! To add a picture follow these simple steps:
                        <ol>
                            <li>Enter App Settings</li>
                            <li>Click the "Manage Questions" button</li>
                            <li>Click on the question you would like to attach a picture to</li>
                            <li>When editing your answer, click on the picture icon and then add an image from your library.</li>
                        </ol>
                    </p>
                    <a href="#" class="fa fa-facebook"></a>
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-google"></a>
                    <a href="#" class="fa fa-youtube"></a>
                </div>
            </div>
        </div>
        <div class="faq-Section">
            <a id="faqQ3">
                <h5 class="panel-title">
                    <span>Can I insert a video in my FAQ?</span>
                    <span class="glyphicon glyphicon-chevron-down pull-right"></span>
                </h5>
            </a>
            <div class="panel-body">
                <div id="faqA3" class="faq-ans">
                    <p>
                        Yes! Users can add video from YouTube or Vimeo with ease:
                        <ol>
                            <li>Enter App Settings.</li>
                            <li>Click the "Manage Questions" button.</li>
                            <li>Click on the question you would like to attach a video to.</li>
                            <li>When editing your answer, click on the video icon and then paste the YouTube or Vimeo video URL.</li>
                            <li>That's it! A thumbnail of your video will appear in answer text box.</li>
                        </ol>
                    </p>
                    <a href="#" class="fa fa-facebook"></a>
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-google"></a>
                    <a href="#" class="fa fa-youtube"></a>
                </div>
            </div>
        </div>
        <div class="faq-Section">
            <a id="faqQ4">
                <h5 class="panel-title">
                    <span>How do I edit or remove the "FAQ title"?</span>
                    <span class="glyphicon glyphicon-chevron-down pull-right"></span>
                </h5>
            </a>
            <div class="panel-body">
                <div id="faqA4" class="faq-ans">
                    <p>The FAQ title can be adjusted in the settings tab of the App Settings. You can also remove the title by unchecking its checkbox in the settings tab.</p>
                    <a href="#" class="fa fa-facebook"></a>
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-google"></a>
                    <a href="#" class="fa fa-youtube"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <img class="img-responsive" src="assets/banners/bonsai-1315488-1278x917.jpg" />
    </div>
</section>
