<!--Home--Section-->
<section id="home">
    <div id="sliderCarousel" class="carousel slide" data-ride="carousel" data-interval="4000">
        <ol class="carousel-indicators">
            <li data-target="#sliderCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#sliderCarousel" data-slide-to="1"></li>
            <li data-target="#sliderCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-caption">
            <form class="search" action="/" method="post">
                <input type="text" name="name" value="" placeholder="Search Here*" />
                <span class="glyphicon glyphicon-search"></span>
            </form>
            <a href="#shop" class="btn btn-buy">SHOP NOW</a>
        </div>
        <div class="carousel-inner">
            <div class="item active">
                <div class="carousel">
                    <img src="assets/banners/b (1).jpg" />
                    <div class="carousel-caption welcome-msg">
                        <h1 class="animated fadeInRight slow anii">Flowers</h1>
                        <h2 class="anii">
                            Happiness Is To Hold Flowers In Both Hands.
                        </h2>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="carousel">
                    <img src="assets/banners/b (2).jpg" />
                    <div class="carousel-caption welcome-msg">
                        <h1 class="animated fadeInRight slow ">Plants</h1>
                        <h2>
                            To Plant A Garden Is To Believe In Towmorrow.
                        </h2>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="carousel">
                    <img src="assets/banners/b (3).jpg" />
                    <div class="carousel-caption welcome-msg">
                        <h1 class="animated fadeInRight slow ">Plants</h1>
                        <h2>
                            Trees And Plants Always Look Like The People They Live With, Somehow
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <a data-target="#sliderCarousel" class="left carousel-control" data-slide="prev">
            <span class="icon-prev"></span>
            <span class="sr-only">PREV</span>
        </a>
        <a data-target="#sliderCarousel" class="right carousel-control" data-slide="next">
            <span class="icon-next"></span>
            <span class="sr-only">NEXT</span>
        </a>
    </div>
</section>
