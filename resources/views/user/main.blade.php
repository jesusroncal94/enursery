@extends('layouts.app', [
    'title' => 'eNursery'
])


@section('content')
    @include('user.home')
    @include('user.shop')
    @include('user.about')
    @include('user.faq')
    @include('user.contact')
@stop


@section('scripts')
@stop
