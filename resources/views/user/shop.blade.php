<!--Shop--Section-->
<section id="shop">
    <!--Categories-->
    <div class="container">
        <div class="col-md-6 col-md-offset-3">
            <div style="margin-top:100px;">
                <h2 class="mainheading">
                    Categories
                </h2>
                <div class="col-lg-8 col-lg-offset-2">
                    <p style="text-align:center">
                        The best categories
                    </p>
                </div>
            </div>
        </div>

        @forelse ($categories as $category)
            <section class="col-lg-4 col-md-4 col-sm-offset-0 col-sm-6 col-xs-offset-2 col-xs-8">
                <div class="psum-item">
                    <a href="{{ route('products') }}">
                        <img class="psum-image img-responsive" src="{{ URL::asset('images/categories/') }}/{{ $category->image }}" alt="product" />
                        <div class="psum-name">
                            <h2>{{ $category->catName }}</h2>
                            <h2>Shop Collection</h2>
                        </div>
                    </a>
                </div>
            </section>
        @empty
            <p>No categories</p>
        @endforelse
    </div>
    <!--New Arrivals-->
    <div class="container">
        <div id="myCarousel" class="carousel slide hidden-sm hidden-xs" data-ride="carousel">
            <!--New Arrivals-->
            <div class="col-md-6 col-md-offset-3">
                <div style="margin-top:100px;">
                    <h2 class="mainheading">
                        New Arrivals
                    </h2>
                    <div class="col-lg-8 col-lg-offset-2">
                        <p style="text-align:center">
                            The best products
                        </p>
                    </div>
                </div>
            </div>

            <!-- Indicators-->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1" class=""></li>
            </ol>

            <!-- Wrapper for slides-->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="">
                        @for( $i = 0; $i < count($products); $i ++ )
                            @if( $i < 3 )
                                <section class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
                                    <div class="psum-item">
                                        <div>
                                            <img class="psum-image img-responsive" src="{{ URL::asset('images/products/') }}/{{ $products[$i]['productImage'] }}" alt="product">
                                            <div class="carousel-caption psum-caption">
                                                <h2>{{ $products[$i]['productName'] }}</h2>
                                                <p>
                                                    <span> Rs. {{ $products[$i]['productPrice'] }}/-</span>
                                                    <span class="clearfix"></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            @endif
                        @endfor
                    </div>
                </div>
                <div class="item">
                    <div class="">
                        @for( $i = 0; $i < count($products); $i ++ )
                            @if( $i >= 3 )
                                <section class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
                                    <div class="psum-item">
                                        <div>
                                            <img class="psum-image img-responsive" src="{{ URL::asset('images/products/') }}/{{ $products[$i]['productImage'] }}" alt="product">
                                            <div class="carousel-caption psum-caption">
                                                <h2>{{ $products[$i]['productName'] }}</h2>
                                                <p>
                                                    <span> Rs. {{ $products[$i]['productPrice'] }}/-</span>
                                                    <span class="clearfix"></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            @endif
                        @endfor
                    </div>
                </div>
            </div>

            <!-- Left and right controls-->
            <div class=" ">
                <a class="carousel-control lg left " data-target="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class=" carousel-control lg right" data-target="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>


    <!--Products-->
    <section class="container">
        @forelse ($products as $product)
        <section class="col-lg-4 col-md-4 col-sm-offset-0 col-sm-6 col-xs-offset-2 col-xs-8">
            <div class="psum-item">
                <div class="image-container">
                    <a onclick="showProductDetails({{ $product->id }});">
                        <img class="psum-image img-responsive" src="{{ URL::asset('images/products') }}/{{ $product->productImage }}" alt="product" />
                    </a>
                    <div class="overlay">
                        <a onclick="showProductDetails({{ $product->id }});" class="btn quickview-btn">QUICK VIEW</a>
                        <button onclick="addToCart({{ $product->id }});" href="" class="btn overlay-btn cart-btn">ADD TO CART</button>
                    </div>
                </div>
                <div class="psum-info">
                    <h2>{{ $product->productName }}</h2>
                    <p>
                        <span class=""> Rs.{{ $product->productPrice }}/-</span>
                    </p>
                </div>
            </div>
        </section>
        @empty
            <p>No products</p>
        @endforelse
        <div class="col-xs-12">
            <a href="{{ route('products') }}" class="btn viewAll">VIEW ALL</a>
        </div>
    </section>


    <div id="details" class="modal text-center">
      	<div class="modal-dialog modal-lg">
        		<div class="modal-content modal-lg animate" method="post">
          			<div class="panel-body">
            				<span style="margin-right:0px; margin-top:-8px; " onclick="document.getElementById('details').style.display='none'" class="close" title="Close Modal">&times;</span>
            				<div class="col-xs-12">
              					<div class="col-xs-5">
                						<img class="img-responsive" src="" id="productImage" />
                						<span>Total: Rs.<span class="t-bill" id="productTotalPrice"></span> /-</span>
              					</div>
              					<div class="col-xs-7" style="margin-top:-10px;">
                						<h3 id="productName"></h3>
                						<h4 class="psum-name-details">Rs.<span class="bill" id="productPrice"></span> /-</h4>
                						<p id="productDescription"></p>
                						<div class="has-feedback form-inline">
                  							<div class="input-group">
                      						  <input id="productQuantity" type="number" min="0" class="form-control cart-quant" value="0" />
                  							</div>
                						</div>
                						<button id="details_btnAddToCart" onclick="" href="" class="btn overlay-btn cart-btn">ADD TO CART</button>
              					</div>
            				</div>
          			</div>
        		</div>
      	</div>
    </div>
</section>
