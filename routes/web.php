<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main view
Route::get('/', 'HomeController@index');

// Products View
Route::get('/products', 'HomeController@products')->name('products');

// User Controller: ajax routes
Route::resource('ajaxusers', 'UserController');
Route::post('ajaxuserslogin', 'UserController@login')->name('ajaxusers.login');
Route::get('ajaxuserslogout', 'UserController@logout')->name('ajaxusers.logout');
Route::post('ajaxusersupdatecart', 'UserController@updateCart')->name('ajaxusers.updateCart');
Route::get('ajaxusersloadcart/{id}', 'UserController@loadCart')->name('ajaxusers.loadCart');

// Role Controller: ajax routes
Route::resource('ajaxroles', 'RoleController');

// Status Controller: ajax routes
Route::resource('ajaxstatuses', 'StatusController');

// Category Controller: ajax routes
Route::resource('ajaxcategories', 'CategoryController');
Route::get('getcategories', 'CategoryController@get')->name('ajaxcategories.get');

// Product Controller: ajax routes
Route::resource('ajaxproducts', 'ProductController');

// Order Controller: ajax routes
Route::resource('ajaxorders', 'OrderController');
Route::post('ajaxordersapproveorder', 'OrderController@approveOrder')->name('ajaxorders.approveOrder');
Route::post('ajaxordersprocessorder', 'OrderController@processOrder')->name('ajaxorders.processOrder');
Route::post('ajaxordersdelivereorder', 'OrderController@delivereOrder')->name('ajaxorders.delivereOrder');

// Dashboard Controller: ajax routes
Route::resource('ajaxdashboards', 'DashboardController');

// Admin view
Route::get('/admin', function () {
    return view('admin.main');
})->middleware(['admin'])->name('admin');

// Partner view
Route::get('/partner', function () {
    return view('pNursuries.main');
})->middleware(['partner'])->name('partner');
